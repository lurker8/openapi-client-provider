package com.acooly.module.openapi.client.provider.alipay;

import com.acooly.core.utils.Strings;

import lombok.Getter;
import lombok.Setter;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author zhangpu@acooly.cn
 */
@ConfigurationProperties(prefix = OpenAPIClientAlipayProperties.PREFIX)
@Getter
@Setter
public class OpenAPIClientAlipayProperties {

    public static final String PREFIX = "acooly.openapi.client.alipay";

    /**
     * 是否启用
     */
    private boolean enable = true;
    
    /**
     * 网关地址
     */
    private String gatewayUrl;
    
    /**
     * appid
     */
    private String appId;

    /**
     * 下载对账文件url
     */
    private String downLoadUrl;

    /**
     * 私钥
     */
    private String privateKey;
    
    /**
     * 支付宝公钥
     */
    private String publicKey;
    
    /**
     * 应用公钥
     */
    private String appPublicKey;

    /**
     * 本系统域名（用于自动生成回调地址）
     */
    private String domain;

    /**
     * 报文中签名字段名称
     */
    private String sign;

    /**
     * 签名方式
     */
    private String signType;

    /**
     * 连接超时时间（毫秒）
     */
    private long connTimeout = 10000;

    /**
     * 读超时时间（毫秒）
     */
    private long readTimeout = 30000;

    /**
     * 商户号
     */
    private String partnerId;

    /**
     * 异步通知URL前缀
     */
    private String notifyUrlPrefix = "/";

    /**
     * 对账文件路径
     */
    private String filePath;

    protected String getCanonicalUrl(String prefix, String postfix) {
        if (Strings.endsWith(prefix, "/")) {
            prefix = Strings.removeEnd(prefix, "/");
        }
        if (Strings.startsWith(postfix, "/")) {
            return prefix + postfix;
        } else {
            return prefix + "/" + postfix;
        }
    }
}
