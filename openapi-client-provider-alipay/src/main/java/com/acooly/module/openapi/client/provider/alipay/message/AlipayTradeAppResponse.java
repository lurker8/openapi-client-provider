package com.acooly.module.openapi.client.provider.alipay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.alipay.domain.AlipayApiMsgInfo;
import com.acooly.module.openapi.client.provider.alipay.domain.AlipayNotify;
import com.acooly.module.openapi.client.provider.alipay.domain.AlipayResponse;
import com.acooly.module.openapi.client.provider.alipay.enums.AlipayServiceEnum;

import lombok.Getter;
import lombok.Setter;

@AlipayApiMsgInfo(service=AlipayServiceEnum.PAY_ALIPAY_TRADE_APP,type=ApiMessageType.Response)
@Setter
@Getter
public class AlipayTradeAppResponse extends AlipayResponse{

	private String payInfo;
}
