package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankHeadRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author zhike 2018/5/24 14:14
 */
@Getter
@Setter
@XStreamAlias("request")
public class WsbankPrePayNoticeNotifyInfo implements Serializable {

    /**
     * 请求报文头
     */
    @XStreamAlias("head")
    private WsbankHeadRequest headRequest;

    /**
     * 请求报文体
     */
    @XStreamAlias("body")
    private WsbankPrePayNoticeNotifyBody requestBody;
}
