/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2017-09-16 16:11 创建
 */
package com.acooly.module.openapi.client.provider.wsbank;

import com.acooly.core.utils.Strings;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static com.acooly.module.openapi.client.provider.wsbank.OpenAPIClientWsbankProperties.PREFIX;

/**
 * @author zhike@acooly.cn
 */
@Getter
@Setter
@ConfigurationProperties(prefix = PREFIX)
public class OpenAPIClientWsbankProperties {
    public static final String PREFIX = "acooly.openapi.client.wsbank";

    /**
     * 网关地址
     */
    private String gatewayUrl;

    /**
     * 图片上传地址
     */
    private String uploadUrl;
    
    /**
     * 交易地址
     */
    private String syncGatewayUrl;

    /**
     * 公钥文件路径
     */
    private String publicKey;

    /**
     * 私钥文件路径
     */
    private String privateKey;

    /**
     * 本系统域名（用于自动生成回调地址）
     */
    private String domain;

    /**
     * 报文中签名字段名称
     */
    private String sign;

    /**
     * 连接超时时间（毫秒）
     */
    private long connTimeout = 10000;

    /**
     * 读超时时间（毫秒）
     */
    private long readTimeout = 30000;

    /**
     * 商户号
     */
    private String partnerId;

    /**
     * 应用ID
     */
    private String appid;

    /**
     * 异步通知URL前缀
     */
    private String notifyUrlPrefix = "/";

    private String notifyUrl = "/wsbank/sdk/notify";

    /**
     * 支付宝APP/WAP支付请求地址
     */
    private String alipayRequestUrl;
    
    /**
     * 支付宝Pid
     */
    private String alipaySource;

    public String getNotifyUrl(String service) {
        return getCanonicalUrl(this.domain, getCanonicalUrl(notifyUrlPrefix, service));
    }

    public String getReturnUrl(String url) {
        return getCanonicalUrl(this.domain, url);
    }


    protected String getCanonicalUrl(String prefix, String postfix) {
        if (Strings.endsWith(prefix, "/")) {
            prefix = Strings.removeEnd(prefix, "/");
        }
        if (Strings.startsWith(postfix, "/")) {
            return prefix + postfix;
        } else {
            return prefix + "/" + postfix;
        }
    }

    private Checkfile checkfile = new Checkfile();

    @Getter
    @Setter
    public static class Checkfile {

        private String host;
        private int port;
        private String username;
        private String password;

        private String serverRoot;
        private String localRoot;

    }

}
