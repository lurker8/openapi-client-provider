package com.acooly.module.openapi.client.provider.wsbank.utils;

import com.acooly.core.utils.StringUtils;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

@Slf4j
public class WsBase64Util {
	
	/**
     * 使用Base64加密算法加密字符串
     *return
     */
    public static String encodeStr(String plainText){
        byte[] b=plainText.getBytes();
        Base64 base64=new Base64();
        b=base64.encode(b);
        String s=new String(b);
        return s;
    }
    
    /**
     * 
     * 使用Base64解密
     *return
     */
    public static String decodeStr(String encodeStr){
        byte[] b=encodeStr.getBytes();
        Base64 base64=new Base64();
        b=base64.decode(b);
        String s=new String(b);
        return s;
    }

	/**
	 * 网商银行base64 加码
	 * @param T
	 * @return
	 */
	public static String encode(Object T ) {
		Base64 encoder = new Base64();
		if (null != T) {
			try {
				return new String(encoder.encode(JSON.toJSONString(T).getBytes()),"UTF-8");
			} catch (UnsupportedEncodingException e) {
				throw new ApiClientException("base64encode异常"+e.getMessage());
			}
		}
		return null;
	}

	/**
	 * 网商银行base64  解码
	 * @param jsonStr
	 * @param T
	 * @return
	 */
	public static <T> T decode(String jsonStr, Class T) {
		if(StringUtils.isEmpty(jsonStr)){
			return null;
		}
		Base64 decoder = new Base64();
		try {
			String classStr = new String(decoder.decode(jsonStr), "UTF-8");
			return (T) JSON.parseObject(classStr,T);
		} catch (UnsupportedEncodingException e) {
			throw new ApiClientException("base64解码异常"+e.getMessage());
		} catch (IOException e) {
			throw new ApiClientException("base64解码异常"+e.getMessage());
		}
	}
	
	/**
	 * 网商银行base64  解码
	 * @param jsonStr
	 * @param T
	 * @return
	 */
	public static <T> T decodeList(String jsonStr, Class T) {
		if(StringUtils.isEmpty(jsonStr)){
			return null;
		}
		Base64 decoder = new Base64();
		try {
			String classStr = new String(decoder.decode(jsonStr), "UTF-8");
			return (T) JSON.parseArray(classStr,T);
		} catch (UnsupportedEncodingException e) {
			throw new ApiClientException("base64解码list异常"+e.getMessage());
		} catch (IOException e) {
			throw new ApiClientException("base64解码list异常"+e.getMessage());
		}
	}
	
	public static void main(String[] args) {
//		WsbankCommonMerchantFeeRate feeParamListObj = new WsbankCommonMerchantFeeRate();
//		feeParamListObj.setChannelType("中国人");
//		feeParamListObj.setFeeType("1233");
//		feeParamListObj.setFeeValue("1000");
//     	System.out.println(WsBase64Util.encode(feeParamListObj));
//		String s = "eyJjaGFubmVsVHlwZSI6IuS4reWbveS6uiIsImZlZVR5cGUiOiIxMjMzIiwiZmVlVmFsdWUiOiIx";
//		WsbankCommonMerchantFeeRate result = WsBase64Util.decode(WsBase64Util.encode(feeParamListObj), WsbankCommonMerchantFeeRate.class);
//		System.out.println(result.getChannelType());
//		String ss ="W3siU2l0ZVVybCI6Ind3dyIsIlNpdGVUeXBlIjoiMDEifSx7IlNpdGVVcmwiOiJ3d3ciLCJTaXRlVHlwZSI6IjAyIn0seyJTaXRlVXJsIjoid3d3IiwiU2l0ZVR5cGUiOiIwMyJ9XQ==";
//		System.out.println(WsBase64Util.decodeStr(ss));
	}
}
