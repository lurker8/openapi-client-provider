package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankHeadRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@XStreamAlias("request")
public class WsbankBkCloudFundsVostroQueryOrderRequestInfo implements Serializable {

	private static final long serialVersionUID = 2564402745518878011L;

	/**
     * 请求报文头
     */
    @XStreamAlias("head")
    private WsbankHeadRequest headRequest;
    
    /**
     * 请求报文体
     */
    @XStreamAlias("body")
    private WsbankBkCloudFundsVostroQueryOrderRequestBody requestBody;
}
