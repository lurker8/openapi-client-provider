package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankHeadResponse;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author sunjx 2018/5/23 14:00
 */
@Getter
@Setter
@XStreamAlias("response")
public class WsbankAlipayResponseInfo implements Serializable {

	private static final long serialVersionUID = 2524829872675393280L;

	/**
     *响应报文头
     */
    @XStreamAlias("head")
    private WsbankHeadResponse headResponse;

    /**
     * 响应报文体
     */
    @XStreamAlias("body")
    private WsbankAlipayResponseBody responseBody;
}
