/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月5日
 *
 */
package com.acooly.module.openapi.client.provider.fuiou.message;

import com.acooly.module.openapi.client.provider.fuiou.domain.FuiouRequest;
import com.acooly.module.openapi.client.provider.fuiou.enums.FuiouServiceEnum;
import com.acooly.module.openapi.client.provider.fuiou.support.FuiouAlias;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 提现 请求报文
 *
 * @author liuyuxiang
 */
public class FuiouWithdrawRequest extends FuiouRequest {

    @NotEmpty
    @Size(min = 1, max = 60)
    @FuiouAlias("login_id")
    private String loginId;

    /**
     * 单位：分
     */
    @Size(min = 1, max = 10)
    @FuiouAlias("amt")
    private String amt;

    @NotEmpty
    @Size(min = 1, max = 200)
    @FuiouAlias("page_notify_url")
    private String returnUrl;

    @NotEmpty
    @Size(min = 1, max = 200)
    @FuiouAlias("back_notify_url")
    private String notifyUrl;

    public FuiouWithdrawRequest() {
        setService(FuiouServiceEnum.Withdraw.getKey());
    }

    /**
     * @param loginId
     * @param amt
     */
    public FuiouWithdrawRequest(String loginId, String amt) {
        this();
        this.loginId = loginId;
        this.amt = amt;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

}
