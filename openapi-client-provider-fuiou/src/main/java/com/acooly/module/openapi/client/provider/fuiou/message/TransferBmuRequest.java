package com.acooly.module.openapi.client.provider.fuiou.message;

import com.acooly.module.openapi.client.provider.fuiou.domain.FuiouRequest;
import com.acooly.module.openapi.client.provider.fuiou.support.FuiouAlias;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 转账 (商户与个人之间) 请求报文
 * 
 * @author liuyuxiang
 */
public class TransferBmuRequest extends FuiouRequest {

	/** 付款登录账户 */
	@NotEmpty
	@Size(min = 1, max = 60)
	@FuiouAlias("out_cust_no")
	private String outCustNo;
	
	/** 收款登录账户 */
	@NotEmpty
	@Size(min = 1, max = 60)
	@FuiouAlias("in_cust_no")
	private String inCustNo;
	
	/** 划拨金额 */
	@NotEmpty
	@Size(min = 1, max = 10)
	@FuiouAlias("amt")
	private String amt;

	/** 预授权合同号 */
	@Size(min = 0, max = 30)
	@FuiouAlias("contract_no")
	private String contractNo;
	
	/** 备注 */
	@Size(min = 0, max = 60)
	@FuiouAlias("rem")
	private String rem;

	public TransferBmuRequest(){}
	
	public TransferBmuRequest(String outCustNo, String inCustNo, String amt, String contractNo, String rem) {
		this.outCustNo = outCustNo;
		this.inCustNo = inCustNo;
		this.contractNo = contractNo;
		this.amt = amt;
		this.rem = rem;
	}

	public String getOutCustNo() {
		return this.outCustNo;
	}

	public void setOutCustNo(String outCustNo) {
		this.outCustNo = outCustNo;
	}

	public String getInCustNo() {
		return this.inCustNo;
	}

	public void setInCustNo(String inCustNo) {
		this.inCustNo = inCustNo;
	}

	public String getAmt() {
		return this.amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	public String getContractNo() {
		return this.contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public String getRem() {
		return this.rem;
	}

	public void setRem(String rem) {
		this.rem = rem;
	}
}
