package com.acooly.module.openapi.client.provider.fuiou.message;

import com.acooly.module.openapi.client.provider.fuiou.domain.FuiouRequest;
import com.acooly.module.openapi.client.provider.fuiou.support.FuiouAlias;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 冻结划拨冻结 请求报文
 * 
 * @author liuyuxiang
 */
public class TransferBuAndFreeze2FreezeRequest extends FuiouRequest {

	/** 付款登录账户 */
	@NotEmpty
	@Size(min = 1, max = 60)
	@FuiouAlias("out_cust_no")
	private String outCustNo;
	
	/** 收款登录账户 */
	@NotEmpty
	@Size(min = 1, max = 60)
	@FuiouAlias("in_cust_no")
	private String inCustNo;
	
	/** 转账金额(分) */
	@NotEmpty
	@Size(min = 0, max = 60)
	@FuiouAlias("amt")
	private String amt;
	
	/** 备注 */
	@Size(min = 0, max = 60)
	@FuiouAlias("rem")
	private String rem;

	public TransferBuAndFreeze2FreezeRequest(){}
	
	public TransferBuAndFreeze2FreezeRequest(String outCustNo, String inCustNo, String amt, String rem) {
		this.outCustNo = outCustNo;
		this.inCustNo = inCustNo;
		this.amt = amt;
		this.rem = rem;
	}

	public String getOutCustNo() {
		return this.outCustNo;
	}

	public void setOutCustNo(String outCustNo) {
		this.outCustNo = outCustNo;
	}

	public String getInCustNo() {
		return this.inCustNo;
	}

	public void setInCustNo(String inCustNo) {
		this.inCustNo = inCustNo;
	}

	public String getAmt() {
		return this.amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	public String getRem() {
		return this.rem;
	}

	public void setRem(String rem) {
		this.rem = rem;
	}
}
