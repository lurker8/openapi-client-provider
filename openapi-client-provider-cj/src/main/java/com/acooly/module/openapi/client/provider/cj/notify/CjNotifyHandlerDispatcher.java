/**
 * coding by zhangpu
 */
package com.acooly.module.openapi.client.provider.cj.notify;

import com.acooly.module.openapi.client.api.ApiServiceClient;
import com.acooly.module.openapi.client.api.notify.AbstractSpringNotifyHandlerDispatcher;
import com.acooly.module.openapi.client.provider.cj.CjApiServiceClient;

import org.springframework.stereotype.Component;

import java.util.Map;

import javax.annotation.Resource;

/**
 * 网关异步通知分发器
 *
 * @author fufeng
 * @date 2016年5月12日
 */
@Component
public class CjNotifyHandlerDispatcher extends AbstractSpringNotifyHandlerDispatcher {

    @Resource(name = "cjApiServiceClient")
    private CjApiServiceClient apiServiceClient;

    @Override
    protected String getServiceKey(String notifyUrl, Map<String, String> notifyData) {
        return notifyData.get("bizType");
    }

    @Override
    protected ApiServiceClient getApiServiceClient() {
        return apiServiceClient;
    }

}
