package com.acooly.module.openapi.client.provider.bosc.message.request;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequestDomain;

@Getter
@Setter
public class SendMobileCodeRequest extends BoscRequestDomain {

	/**
	 * 手机号
	 */
	@NotBlank
	private String mobile;

}
