/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月6日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.trade.dto;

import com.acooly.module.openapi.client.provider.sinapay.annotation.ApiDto;
import com.acooly.module.openapi.client.provider.sinapay.annotation.ItemOrder;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayAccountType;
import com.acooly.module.openapi.client.provider.sinapay.message.Dtoable;
import lombok.Getter;
import lombok.Setter;

/**
 * 分润项信息
 * 
 * @author zhike
 */
@Getter
@Setter
@ApiDto
public class TradePayItem implements Dtoable {

	@ItemOrder(0)
	private String payerId;
	@ItemOrder(1)
	private String payerIdentityType = "UID";
	@ItemOrder(2)
	private String payerAccountType = SinapayAccountType.SAVING_POT.code();
	@ItemOrder(3)
	private String payeeId;
	@ItemOrder(4)
	private String payeeIdentityType = "UID";
	@ItemOrder(5)
	private String payeeAccountType = SinapayAccountType.SAVING_POT.code();
	@ItemOrder(6)
	private String amount;
	@ItemOrder(7)
	private String memo;

	public TradePayItem() {
		super();
	}

	public TradePayItem(String payerId, String payeeId, String amount, String memo) {
		super();
		this.payerId = payerId;
		this.payeeId = payeeId;
		this.amount = amount;
		this.memo = memo;
	}
}
