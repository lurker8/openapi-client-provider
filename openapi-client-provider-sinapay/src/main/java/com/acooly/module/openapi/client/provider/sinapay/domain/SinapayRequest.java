/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2018-01-23 12:58 创建
 */
package com.acooly.module.openapi.client.provider.sinapay.domain;

import com.acooly.core.utils.Dates;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author zhike 2018-01-23 12:58
 */
@Getter
@Setter
public class SinapayRequest extends SinapayMessage {

    @NotEmpty
    @ApiItem
    private String version = "1.2";

    /** 请求时间 yyyyMMddHHmmss */
    @NotEmpty
    @ApiItem(value = "request_time")
    private String requestTime = Dates.format(new Date(), "yyyyMMddHHmmss");

    @ApiItem("_input_charset")
    private String inputCharset = "UTF-8";

    @ApiItem(value = "sign", sign = false)
    private String sign;
    @ApiItem(value = "sign_type", sign = false)
    private String signType = "RSA";
    @ApiItem(value = "sign_version", sign = false)
    private String signVersion;

    @ApiItem("encrypt_version")
    private String encryptVersion;

    @ApiItem("notify_url")
    private String notifyUrl;

    @ApiItem("return_url")
    private String returnUrl;

    private String memo;

    /**
     * 扩展信息
     */
    @Size(max = 200)
    @ApiItem("extend_param")
    private String extendParam;

    /**
     * 收银台地址类别
     *
     * 收银台地址类型，目前只包含MOBILE。为空时默认返回PC版页面，当传值为“MOBILE”时返回移动版页面。
     */
    @Size(max = 10)
    @ApiItem("cashdesk_addr_category")
    private String cashdeskAddrCategory;
}
