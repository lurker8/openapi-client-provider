/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月1日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.member.dto;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.provider.sinapay.annotation.ApiDto;
import com.acooly.module.openapi.client.provider.sinapay.annotation.ItemOrder;
import com.acooly.module.openapi.client.provider.sinapay.message.Dtoable;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike
 */
@ApiDto
@Getter
@Setter
public class SinapayTradeInfo implements Dtoable {

	/** 摘要 */
	@ItemOrder(0)
	private String digest;

	/**
	 * 入账时间
	 */
	@ItemOrder(1)
	private String paymentReceivingTime;

	/**
	 * 发生方向，加（+）、减（-）
	 */
	@ItemOrder(2)
	private String addAndSubtractType;

	/** 最近一月收益 */
	@MoneyConstraint
	@ItemOrder(3)
	private Money amount;

	/** 总收益 */
	@MoneyConstraint
	@ItemOrder(4)
	private Money balance;

	/**
	 * 业务类型
	 */
	@ItemOrder(5)
	private String businessType;
}
