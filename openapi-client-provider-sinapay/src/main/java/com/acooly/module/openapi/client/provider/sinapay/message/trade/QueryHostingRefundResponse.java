package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.annotation.SinaGateWay;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayGateWayType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import com.acooly.module.openapi.client.provider.sinapay.message.trade.dto.TradeItem;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 退款查询
 *
 * @author xiaohong
 * @create 2018-07-17 17:32
 **/
@Getter
@Setter
@SinaGateWay(SinapayGateWayType.PAYMENT)
@SinapayApiMsg(service = SinapayServiceNameEnum.QUERY_HOSTING_REFUND, type = ApiMessageType.Response)
public class QueryHostingRefundResponse extends SinapayResponse {
    /**
     * 交易明细列表
     */
    @ApiItem(value = "trade_list")
    private List<TradeItem> tradeList = Lists.newArrayList();

    /**
     * 页号
     */
    @ApiItem(value = "page_no")
    private int pageNo;

    /**
     * 每页大小
     */
    @ApiItem(value = "page_size")
    private int pageSize;

    /**
     * 总记录数
     */
    @ApiItem(value = "total_item")
    private int totalItem;
}
