/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月5日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.core.utils.Ids;
import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.annotation.SinaGateWay;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayAccountType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayGateWayType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike
 */
@Getter
@Setter
@SinaGateWay(SinapayGateWayType.PAYMENT)
@SinapayApiMsg(service = SinapayServiceNameEnum.CREATE_HOSTING_WITHDRAW, type = ApiMessageType.Request)
public class CreateHostingWithdrawRequest extends SinapayRequest {

	/**
	 * 交易订单号
	 *
	 * 商户网站交易订单号，商户内部保证唯一
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "out_trade_no")
	private String outTradeNo = Ids.oid();

	/**
	 * 摘要
	 *
	 * 提现内容摘要
	 */
	@Size(max = 64)
	@ApiItem(value = "summary")
	private String summary;

	/**
	 * 用户标识信息
	 *
	 * 商户系统用户ID(字母或数字)
	 */
	@NotEmpty
	@Size(max = 50)
	@ApiItem(value = "identity_id")
	private String identityId;

	/**
	 * 用户标识类型
	 *
	 * ID的类型，参考“标志类型”
	 */
	@NotEmpty
	@Size(max = 16)
	@ApiItem(value = "identity_type")
	private String identityType = "UID";

	/**
	 * 账户类型
	 *
	 * 账户类型（基本户、存钱罐、银行账户）。默认基本户，见附录
	 */
	@Size(max = 16)
	@ApiItem(value = "account_type")
	private String accountType = SinapayAccountType.SAVING_POT.code();

	/**
	 * 金额
	 *
	 * 单位为：RMB Yuan。精确到小数点后两位。
	 */
	@MoneyConstraint
	@ApiItem(value = "amount")
	private Money amount;

	/**
	 * 用户手续费
	 *
	 * 用户承担的手续费金额
	 */
	@MoneyConstraint(nullable = true)
	@ApiItem(value = "user_fee")
	private Money userFee;

	/**
	 * 银行卡ID
	 *
	 * 用户绑定银行卡ID，即绑定银行卡返回的ID，如果走收银台可为空
	 */
	@Size(max = 32)
	@ApiItem(value = "card_id")
	private String cardId;

	/**
	 * 提现方式
	 *
	 * 安全模式商户需要填写CASHDESK既跳转新浪支付资金托管站点进行提现。
	 */
	@Size(max = 16)
	@ApiItem(value = "withdraw_mode")
	private String withdrawMode;

	/**
	 * 到账类型
	 *
	 * GENERAL: 普通; FAST: 快速
	 */
	@Size(max = 16)
	@ApiItem(value = "payto_type")
	private String paytoType;

	/**
	 * 提现关闭时间
	 *
	 * 设置未付款出款提现交易的超时时间，一旦超时，该笔交易就会自动被关闭。
	 * 取值范围：1m～15m。m-分钟，h-小时 不接受小数点 默认为15分钟
	 */
	@Size(max = 8)
	@ApiItem(value = "withdraw_close_time")
	private String withdrawCloseTime;

	/**
	 * 用户IP/运营商IP
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "user_ip")
	private String userIp;
	
	public CreateHostingWithdrawRequest() {
		super();
	}

	/**
	 * @param identityId
	 * @param amount
	 */
	public CreateHostingWithdrawRequest(String identityId, Money amount,String userIp) {
		this();
		this.identityId = identityId;
		this.amount = amount;
		this.userIp = userIp;
	}
}
