package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayAccountType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/7/10 14:13
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.BALANCE_FREEZE, type = ApiMessageType.Request)
public class BalanceFreezeRequest extends SinapayRequest {

    /**
     *冻结订单号
     * 商户网站冻结订单号，商户内部保证唯一
     */
    @NotEmpty
    @Size(max = 32)
    @ApiItem(value = "out_freeze_no")
    private String outFreezeNo;

    /**
     *用户标识信息
     * 商户系统用户ID(字母或数字)
     */
    @NotEmpty
    @Size(max = 50)
    @ApiItem(value = "identity_id")
    private String identityId;

    /**
     *用户标识类型
     * ID的类型，目前只包括UID
     */
    @NotEmpty
    @Size(max = 16)
    @ApiItem(value = "identity_type")
    private String identityType = "UID";

    /**
     *账户类型
     * 账户类型（基本户、保证金户）。默认基本户，见附录
     */
    @Size(max = 16)
    @ApiItem(value = "account_type")
    private String accountType = SinapayAccountType.SAVING_POT.getCode();

    /**
     *金额
     * 单位为：RMB Yuan。精确到小数点后两位。
     */
    @MoneyConstraint
    @ApiItem(value = "amount")
    private Money amount;

    /**
     *摘要
     * 请求摘要
     */
    @NotEmpty
    @Size(max = 64)
    @ApiItem(value = "summary")
    private String summary;

    /**
     *请求者IP
     * 用户在商户平台操作时候的IP地址，公网IP，不是内网IP
     * 用于风控校验，请填写用户真实IP，否则容易风控拦截
     */
    @NotEmpty
    @Size(max = 50)
    @ApiItem(value = "client_ip")
    private String clientIp;
}
