package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author zhike 2018/7/10 15:41
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.QUERY_FUND_QUOTA, type = ApiMessageType.Response)
public class QueryFundQuotaResponse extends SinapayResponse {
    /**
     *总额度
     * 单位为：RMB Yuan。精确到小数点后两位。
     */
    @NotNull
    @MoneyConstraint
    @ApiItem(value = "all_balance")
    private Money allBalance;

    /**
     *用额度
     * 单位为：RMB Yuan。精确到小数点后两位。
     */
    @NotNull
    @MoneyConstraint
    @ApiItem(value = "available_balance")
    private Money availableBalance;
    
    /**
     *限制标记
     * 无限额用户。Y有限制，N无限制。有限制时又可用额度决定
     */
    @NotEmpty
    @Size(max = 5)
    @ApiItem(value = "limit")
    private String limit;


}
