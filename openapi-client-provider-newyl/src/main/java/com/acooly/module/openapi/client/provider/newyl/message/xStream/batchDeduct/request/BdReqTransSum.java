package com.acooly.module.openapi.client.provider.newyl.message.xStream.batchDeduct.request;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * @author fufeng 2018/1/26 15:29.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("TRANS_SUM")
public class BdReqTransSum {
    /**
     *业务代码
     */
    @XStreamAlias("BUSINESS_CODE")
    private String businessCode;
    /**
     *商户代码
     */
    @XStreamAlias("MERCHANT_ID")
    private String merchantId;
    /**
     *预约发送时间
     */
    @XStreamAlias("SUBMIT_TIME")
    private String submitTime;
    /**
     *总记录数
     */
    @XStreamAlias("TOTAL_ITEM")
    private String totalItem;
    /**
     *总金额
     */
    @XStreamAlias("TOTAL_SUM")
    private String totalSum;

}
