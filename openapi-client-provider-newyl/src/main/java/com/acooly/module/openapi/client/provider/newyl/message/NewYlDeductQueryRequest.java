package com.acooly.module.openapi.client.provider.newyl.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.newyl.domain.NewYlRequest;
import com.acooly.module.openapi.client.provider.newyl.domain.NewYlApiMsgInfo;
import com.acooly.module.openapi.client.provider.newyl.enums.NewYlServiceEnum;
import com.acooly.module.openapi.client.provider.newyl.message.xStream.common.ReqInfo;
import com.acooly.module.openapi.client.provider.newyl.message.xStream.deductQuery.request.QdReqBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Getter;
import lombok.Setter;

/**
 * @author fufeng
 */
@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("GZZF")
@NewYlApiMsgInfo(service = NewYlServiceEnum.NEW_YL_DEDUCT_QUERY, type = ApiMessageType.Request)
public class NewYlDeductQueryRequest extends NewYlRequest {

    @XStreamAlias("INFO")
    private ReqInfo reqInfo;
    @XStreamAlias("BODY")
    private QdReqBody qdReqBody;

}
