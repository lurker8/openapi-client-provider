package com.acooly.module.openapi.client.provider.newyl.message.xStream.deductQuery.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * @author fufeng 2018/1/26 15:26.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("QUERY_TRANS")
public class QdRespQueryTrans {
    /**
     *要查询的交易流水
     */
    @XStreamAlias("QUERY_SN")
    private String querySn;
    /**
     *查询备注
     */
    @XStreamAlias("QUERY_REMARK")
    private String queryRemark;

}
