package com.acooly.module.openapi.client.provider.newyl;

import com.acooly.module.openapi.client.api.transport.HttpTransport;
import com.acooly.module.openapi.client.api.transport.Transport;
import org.apache.http.entity.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;


@EnableConfigurationProperties({NewYlProperties.class})
@ConditionalOnProperty(value = NewYlProperties.PREFIX + ".enable", matchIfMissing = true)
@ComponentScan
public class NewYlConfigration {

    @Autowired
    private NewYlProperties newYlProperties;

    @Bean("newYlHttpTransport")
    public Transport weBankHttpTransport() {
        HttpTransport httpTransport = new HttpTransport();
        //httpTransport.setGateway(ylProperties.getGatewayUrl());
        httpTransport.setConnTimeout(String.valueOf(newYlProperties.getConnTimeout()));
        httpTransport.setReadTimeout(String.valueOf(newYlProperties.getReadTimeout()));
        httpTransport.setCharset("GBK");
        httpTransport.setContentType(ContentType.TEXT_PLAIN.getMimeType());
        return httpTransport;
    }
}
