/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.jyt.domain;

import com.acooly.module.openapi.client.provider.jyt.message.dto.JytHeaderRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/** @author zhangpu */
@Getter
@Setter
public class JytRequest extends JytApiMessage {

  /**
   * 请求报文头信息
   */
  @XStreamAlias("head")
  private JytHeaderRequest headerRequest;

}
