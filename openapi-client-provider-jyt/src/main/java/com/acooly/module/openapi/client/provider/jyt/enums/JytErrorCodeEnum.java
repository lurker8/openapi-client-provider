/*
 * ouwen@yiji.com Inc.
 * Copyright (c) 2017 All Rights Reserved.
 * create by ouwen
 * date:2017-03-30
 *
 */
package com.acooly.module.openapi.client.provider.jyt.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * inst_channel_api DebitCreditType 枚举定义
 *
 * @author zhike
 * Date: 2017-03-30 15:12:06
 */
public enum JytErrorCodeEnum implements Messageable {

    SYSTEM_ERROR("SYSTEM_ERROR", "接口返回错误"),
    INVALID_PARAMETER("INVALID_PARAMETER", "银行卡信息错误"),
    ACCESS_FORBIDDEN("ACCESS_FORBIDDEN", "无权限使用接口"),
    EXIST_FORBIDDEN_WORD("EXIST_FORBIDDEN_WORD", "订单信息中包含违禁词"),
    TOTAL_FEE_EXCEED("TOTAL_FEE_EXCEED", "订单总金额超过限额"),
    CONTEXT_INCONSISTENT("CONTEXT_INCONSISTENT", "交易信息被篡改"),
    TRADE_HAS_SUCCESS("TRADE_HAS_SUCCESS", "交易已被支付"),
    TRADE_HAS_CLOSE("TRADE_HAS_CLOSE", "交易已经关闭"),
    BUYER_SELLER_EQUAL("BUYER_SELLER_EQUAL", "买卖家不能相同"),
    TRADE_BUYER_NOT_MATCH("TRADE_BUYER_NOT_MATCH", "交易买家不匹配"),
    BUYER_ENABLE_STATUS_FORBID("BUYER_ENABLE_STATUS_FORBID", "买家状态非法"),
    SELLER_BEEN_BLOCKED("SELLER_BEEN_BLOCKED", "商家账号被冻结"),
    ERROR_BUYER_CERTIFY_LEVEL_LIMIT("ERROR_BUYER_CERTIFY_LEVEL_LIMIT", "买家未通过人行认证"),
    ;

    private final String code;
    private final String message;

    private JytErrorCodeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (JytErrorCodeEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static JytErrorCodeEnum find(String code) {
        for (JytErrorCodeEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<JytErrorCodeEnum> getAll() {
        List<JytErrorCodeEnum> list = new ArrayList<JytErrorCodeEnum>();
        for (JytErrorCodeEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (JytErrorCodeEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }

}
