/**
 * create by zhangpu
 * date:2015年3月12日
 */
package com.acooly.module.openapi.client.provider.jyt.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.message.MessageFactory;
import com.acooly.module.openapi.client.provider.jyt.JytConstants;
import com.acooly.module.openapi.client.provider.jyt.domain.JytResponse;
import com.acooly.module.openapi.client.provider.jyt.enums.JytServiceEnum;
import com.acooly.module.openapi.client.provider.jyt.exception.JytApiClientProcessingException;
import com.acooly.module.openapi.client.provider.jyt.utils.JsonMarshallor;
import com.acooly.module.openapi.client.provider.jyt.utils.StringHelper;
import com.acooly.module.openapi.client.provider.jyt.utils.XmlUtils;
import com.acooly.module.safety.Safes;
import com.acooly.module.safety.key.KeyLoadManager;
import com.acooly.module.safety.signature.SignTypeEnum;
import com.acooly.module.safety.support.KeyStoreInfo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author zhangpu
 */
@Service
@Slf4j
public class JytResponseUnmarshall extends JytMarshallSupport implements ApiUnmarshal<JytResponse, String> {

    private static final Logger logger = LoggerFactory.getLogger(JytResponseUnmarshall.class);

    @Resource(name = "jytMessageFactory")
    private MessageFactory messageFactory;

    private static JsonMarshallor jsonMarshallor = JsonMarshallor.INSTANCE;

    @Autowired
    private KeyLoadManager keyStoreLoadManager;


    @SuppressWarnings("unchecked")
    @Override
    public JytResponse unmarshal(String message, String serviceName) {
        try {
            logger.info("响应报文密文:{}", message);
            // 验签
            Map<String,String> responseMap = StringHelper.getResponseParm(message);
            String signature = responseMap.get(JytConstants.SIGN);
            String xmlEnc = responseMap.get(JytConstants.XML_ENC);
            String keyEnc = responseMap.get(JytConstants.KEY_ENC);
            String partnerId = responseMap.get(JytConstants.MERCHANT_NO);
            //解密响应报文
            String respXml = decryptKey(keyEnc,xmlEnc);
            logger.info("响应报文明文:{}", respXml);
            KeyStoreInfo keyStoreInfo = keyStoreLoadManager.load(partnerId, JytConstants.PROVIDER_NAME);
            Safes.getSigner(SignTypeEnum.Cert).verify(respXml,keyStoreInfo,signature);
            log.info("验签成功");
            return doUnmarshall(respXml, serviceName);
        } catch (Exception e) {
            throw new JytApiClientProcessingException("解析响应报文异常"+e.getMessage());
        }
    }

    protected JytResponse doUnmarshall(String respXml, String serviceName) {
        JytResponse response = (JytResponse) messageFactory.getResponse(JytServiceEnum.find(serviceName).getCode());
        response = XmlUtils.toBean(respXml,response.getClass());
        response.setService(JytServiceEnum.find(serviceName).getCode());
        return response;
    }

    public void setMessageFactory(MessageFactory messageFactory) {
        this.messageFactory = messageFactory;
    }


}
