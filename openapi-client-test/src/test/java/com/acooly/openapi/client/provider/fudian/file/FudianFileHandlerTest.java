package com.acooly.openapi.client.provider.fudian.file;

import com.acooly.core.common.BootApp;
import com.acooly.module.openapi.client.file.FileDataHandler;
import com.acooly.module.openapi.client.file.domain.FileHandlerOrder;
import com.acooly.module.openapi.client.provider.fudian.file.FudianSftpFileHandler;
import com.acooly.openapi.client.provider.fudian.FudianAbstractTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@Slf4j
@SpringBootApplication
@BootApp(sysName = "test")
public class FudianFileHandlerTest extends FudianAbstractTest {

    @Autowired
    FudianSftpFileHandler fudianSftpFileHandler;


    /**
     * 硬编码方式处理文件
     */
    @Test
    public void downloadHandleWithCodingTest() {

        FileHandlerOrder order = new FileHandlerOrder("20180210", "recharge");
        order.put("fileName", "recharge_20180210");
        fudianSftpFileHandler.downloadHandle(order, new FileDataHandler() {
            @Override
            public void handle(String period, int index, List<String> rows, boolean finished) {
                log.info("page {}, period: {}, finished:{}", index, period, finished);
                for (String row : rows) {
                    log.info(row);
                }
                log.info("");
            }

            @Override
            public int getBatchSize() {
                return 10;
            }

            @Override
            public String name() {
                return "recharge";
            }
        });
    }


    @Test
    public void downloadHandleWithSpringTest() {

        FileHandlerOrder order = new FileHandlerOrder("20180303", "recharge");
        order.put("fileName", "recharge_20180210");
        fudianSftpFileHandler.downloadHandle(order);
    }

}
