package com.acooly.openapi.client.provider.wsbank;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.wsbank.WsbankApiService;
import com.acooly.module.openapi.client.provider.wsbank.message.*;
import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankCommonMerchantFeeRate;
import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankSiteInfo;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.*;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@BootApp(sysName = "wsbankTest")
public class WsbankTest extends NoWebTestBase {
    @Autowired
    private WsbankApiService wsbankApiService;

    /**
     * 测试短息发送接口
     */
    @Test
    public void sendSmsCode() {
        WsbankSendSmsCodeRequest request = new WsbankSendSmsCodeRequest();
        WsbankSendSmsCodeRequestInfo requestInfo = new WsbankSendSmsCodeRequestInfo();
        WsbankSendSmsCodeRequestBody requestBody = new WsbankSendSmsCodeRequestBody();
        requestBody.setOutTradeNo(Ids.oid());
//        requestBody.setMerchantId("226801000000000261290");
        requestBody.setBizType("04");
        requestBody.setMobileNo("18679875227");
        requestInfo.setRequestBody(requestBody);
        request.setRequestInfo(requestInfo);
        WsbankSendSmsCodeResponse response = wsbankApiService.sendSmsCode(request);
        System.out.println("短息发送接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 测试订单查询接口
     */
    @Test
    public void payQuery() {
        WsbankPayQueryRequest request = new WsbankPayQueryRequest();
        WsbankPayQueryRequestInfo requestInfo = new WsbankPayQueryRequestInfo();
        WsbankPayQueryRequestBody requestBody = new WsbankPayQueryRequestBody();
        requestBody.setOutTradeNo("o18053114334111200001");
        requestBody.setMerchantId("226801000000103460124");
        requestInfo.setRequestBody(requestBody);
        request.setRequestInfo(requestInfo);
        WsbankPayQueryResponse response = wsbankApiService.payQuery(request);
        System.out.println("创建支付订单接口响应报文："+ JSON.toJSONString(response));
    }
    
    @Test
    public void registerQuery() {
        WsbankRegisterQueryRequest request = new WsbankRegisterQueryRequest();
        WsbankRegisterQueryRequestInfo requestInfo = new WsbankRegisterQueryRequestInfo();
        WsbankRegisterQueryRequestBody requestBody = new WsbankRegisterQueryRequestBody();
        requestBody.setOrderNo("2018052811150710010000000000000000124032");
        requestInfo.setRequestBody(requestBody);
        request.setRequestInfo(requestInfo);
        WsbankRegisterQueryResponse response = wsbankApiService.registerQuery(request);
        System.out.println("查询商家入驻接口响应报文："+ JSON.toJSONString(response));
    }

    @Test
    public void addMerchantConfig() {
        WsbankAddMerchantConfigRequest request = new WsbankAddMerchantConfigRequest();
        WsbankAddMerchantConfigRequestInfo requestInfo = new WsbankAddMerchantConfigRequestInfo();
        WsbankAddMerchantConfigRequestBody requestBody = new WsbankAddMerchantConfigRequestBody();
        requestBody.setOutTradeNo(Ids.oid());
        requestBody.setMerchantId("226801000000103460124");
        requestBody.setAppid("wx69cb7dbc3f9b9032");
        //requestBody.setPath("www.baidu.com");
        requestBody.setSubscribeAppid("gh_9b2675fc9208");
        requestInfo.setRequestBody(requestBody);
        request.setRequestInfo(requestInfo);
        WsbankAddMerchantConfigResponse response = wsbankApiService.addMerchantConfig(request);
        System.out.println("微信子商户支付配置接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 测试图片上传接口
     * @throws Exception
     */
    @Test
    public void testUpload() {
        WsbankUploadImgRequest request = new WsbankUploadImgRequest();
        request.setPhotoType("01");
        File picIn = new File("d:/a.jpg");
        request.setPicture(picIn);
        request.setOutTradeNo(Ids.oid());
        WsbankUploadImgResponse response = wsbankApiService.uploadImg(request);
        System.out.println("图片上传接口响应报文："+ JSON.toJSONString(response));
    }

	 @Test
	 public void testMerchantQuery(){
		    WsbankMerchantQueryRequest request = new WsbankMerchantQueryRequest();
		    WsbankMerchantQueryRequestInfo requestInfo = new WsbankMerchantQueryRequestInfo();
	        WsbankMerchantQueryRequestBody requestBody = new WsbankMerchantQueryRequestBody();
	        requestBody.setMerchantId("226801000000103460124");
	        requestInfo.setRequestBody(requestBody);
	        request.setRequestInfo(requestInfo);
	        WsbankMerchantQueryResponse response = wsbankApiService.merchantQuery(request);
	        System.out.println("商户信息查询接口响应报文："+JSON.toJSONString(response));
		 
	 }
	 
	 @Test
	 public void testMerchantUpdate(){
		    WsbankMerchantUpdateRequest request = new WsbankMerchantUpdateRequest();
		    WsbankMerchantUpdateRequestInfo requestInfo = new WsbankMerchantUpdateRequestInfo();
	        WsbankMerchantUpdateRequestBody requestBody = new WsbankMerchantUpdateRequestBody();
	        requestBody.setMerchantId("226801000000103460124");
	        
//	        WsbankCommonMerchantDetail  detail = new WsbankCommonMerchantDetail();
//	        detail.setAddress("盐城");
//	        detail.setEmail("89662112@qq.com");
//	        requestBody.setMerchantDetailObj(detail);
	        List<WsbankSiteInfo> wsbankSiteInfoList = new ArrayList<WsbankSiteInfo>();
	        WsbankSiteInfo siteInfo  = new WsbankSiteInfo();
	        siteInfo.setSiteType("04");
	        siteInfo.setSiteUrl("WWW");
	        wsbankSiteInfoList.add(siteInfo);
	        
	        
	        requestBody.setWsbankSiteInfoObj(wsbankSiteInfoList);
	        
	     	//手续费信息
	    	List<WsbankCommonMerchantFeeRate> wsbankCommonMerchantFeeRateList = new ArrayList<WsbankCommonMerchantFeeRate>();
	    	WsbankCommonMerchantFeeRate wsbankCommonMerchantFeeRate1 = new WsbankCommonMerchantFeeRate();
	    	wsbankCommonMerchantFeeRate1.setChannelType("01");
	    	wsbankCommonMerchantFeeRate1.setFeeType("02");
	    	wsbankCommonMerchantFeeRate1.setFeeValue("0.0035");
	    	wsbankCommonMerchantFeeRateList.add(wsbankCommonMerchantFeeRate1);
	    	WsbankCommonMerchantFeeRate wsbankCommonMerchantFeeRate2 = new WsbankCommonMerchantFeeRate();
	    	wsbankCommonMerchantFeeRate2.setChannelType("02");
	    	wsbankCommonMerchantFeeRate2.setFeeType("02");
	    	wsbankCommonMerchantFeeRate2.setFeeValue("0.0035");
	    	wsbankCommonMerchantFeeRateList.add(wsbankCommonMerchantFeeRate2);
	    	WsbankCommonMerchantFeeRate wsbankCommonMerchantFeeRate3 = new WsbankCommonMerchantFeeRate();
	    	wsbankCommonMerchantFeeRate3.setChannelType("05");
	    	wsbankCommonMerchantFeeRate3.setFeeType("02");
	    	wsbankCommonMerchantFeeRate3.setFeeValue("0.0035");
	    	wsbankCommonMerchantFeeRateList.add(wsbankCommonMerchantFeeRate3);
	        requestBody.setFeeParamListObj(wsbankCommonMerchantFeeRateList);
	        
	        requestBody.setMcc("2015050700000000");
	        requestBody.setOnlineMcc("5411");
	        requestBody.setDealType("03");
	        requestBody.setOutTradeNo(Ids.oid());
	        requestInfo.setRequestBody(requestBody);
	        request.setRequestInfo(requestInfo);
	        WsbankMerchantUpdateResponse response = wsbankApiService.merchantUpdate(request);
	        System.out.println("商户信息修改接口响应报文："+JSON.toJSONString(response));
	 }
	 
	 @Test
	 public void testMerchantFreeze(){
		    WsbankMerchantFreezeRequest request = new WsbankMerchantFreezeRequest();
		    WsbankMerchantFreezeRequestInfo requestInfo = new WsbankMerchantFreezeRequestInfo();
	        WsbankMerchantFreezeRequestBody requestBody = new WsbankMerchantFreezeRequestBody();
	        requestBody.setMerchantId("226801000000103460124");
	        requestBody.setOutTradeNo(Ids.oid());
	        requestBody.setFreezeReason("this is a reason");
	        requestInfo.setRequestBody(requestBody);
	        request.setRequestInfo(requestInfo);
	        WsbankMerchantFreezeResponse response = wsbankApiService.merchantFreeze(request);
	        System.out.println("商户冻结接口响应报文："+JSON.toJSONString(response));
	 }
	 
	 
	 @Test
	 public void testMerchantUnFreeze(){
		    WsbankMerchantUnFreezeRequest request = new WsbankMerchantUnFreezeRequest();
		    WsbankMerchantUnFreezeRequestInfo requestInfo = new WsbankMerchantUnFreezeRequestInfo();
	        WsbankMerchantUnFreezeRequestBody requestBody = new WsbankMerchantUnFreezeRequestBody();
	        requestBody.setMerchantId("226801000000103460124");
	        requestBody.setOutTradeNo(Ids.oid());
	        requestBody.setUnfreezeReason("this is an unFreeze reason");
	        requestInfo.setRequestBody(requestBody);
	        request.setRequestInfo(requestInfo);
	        WsbankMerchantUnFreezeResponse response = wsbankApiService.merchantUnFreeze(request);
	        System.out.println("商户开启接口响应报文："+JSON.toJSONString(response));
	 }
	 
	 @Test
	 public void bkmerchanttradePay(){
		 WsbankBkmerchanttradePayRequest request = new WsbankBkmerchanttradePayRequest();
		 	WsbankBkmerchanttradePayRequestInfo requestInfo = new WsbankBkmerchanttradePayRequestInfo();
		 	WsbankBkmerchanttradePayRequestBody requestBody = new WsbankBkmerchanttradePayRequestBody();
	        requestBody.setMerchantId("226801000000103460124");
	        requestBody.setOutTradeNo(Ids.oid());
	        requestBody.setAuthCode("284227574320233998");
	        requestBody.setBody("商城-电视");
	        requestBody.setTotalAmount("1");
	        requestBody.setCurrency("CNY");
	        requestBody.setChannelType("ALI");
	        requestBody.setDeviceCreateIp("192.168.2.131");
	        requestBody.setSettleType("T1");
	        requestInfo.setRequestBody(requestBody);
	        request.setRequestInfo(requestInfo);
	        WsbankBkmerchanttradePayResponse response = wsbankApiService.bkmerchanttradePay(request);
	        System.out.println("移动刷卡支付（被扫）接口响应报文："+JSON.toJSONString(response));
	 }
	 
	 @Test
	 public void bkmerchanttradePrePay(){
		 	WsbankBkmerchanttradePrePayRequest request = new WsbankBkmerchanttradePrePayRequest();
		 	WsbankBkmerchanttradePrePayRequestInfo requestInfo = new WsbankBkmerchanttradePrePayRequestInfo();
		 	WsbankBkmerchanttradePrePayRequestBody requestBody = new WsbankBkmerchanttradePrePayRequestBody();
	        requestBody.setMerchantId("226801000000103688124");
	        requestBody.setOutTradeNo(Ids.oid());
	       // requestBody.setAuthCode("287848237941595064");
	        requestBody.setBody("商城-电视1");
	        requestBody.setTotalAmount("1");
	        requestBody.setOpenId("2088702185633351");
	        requestBody.setCurrency("CNY");
	        requestBody.setChannelType("WX");
	        requestBody.setDeviceCreateIp("192.168.2.131");
	        requestBody.setSettleType("T1");
	        requestInfo.setRequestBody(requestBody);
	        request.setRequestInfo(requestInfo);
	        WsbankBkmerchanttradePrePayResponse response = wsbankApiService.bkmerchanttradePrePay(request);
	        System.out.println("移动刷卡支付（主扫）接口响应报文："+JSON.toJSONString(response));
	 }
	 
	 @Test
	 public void testVostroQueryOrder(){
		 WsbankBkCloudFundsVostroQueryOrderRequest request = new WsbankBkCloudFundsVostroQueryOrderRequest();
		 WsbankBkCloudFundsVostroQueryOrderRequestInfo requestInfo = new WsbankBkCloudFundsVostroQueryOrderRequestInfo();
		 WsbankBkCloudFundsVostroQueryOrderRequestBody requestBody = new WsbankBkCloudFundsVostroQueryOrderRequestBody();
	     requestBody.setMerchantId("226801000000103460124");
	     requestBody.setOutTradeNo("lsh1111111111111");
	     requestBody.setOutTradeNo("testOutTradeNo11111");
	     requestBody.setOrderNo("testOrderNo11111");
	     requestInfo.setRequestBody(requestBody);
	     request.setRequestInfo(requestInfo);
	     WsbankBkCloudFundsVostroQueryOrderResponse response = wsbankApiService.vostroQueryOrder(request);
	     System.out.println("单笔来帐汇入订单查询接口响应报文："+JSON.toJSONString(response));
	 }
}
