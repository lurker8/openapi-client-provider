package com.acooly.openapi.client.provider.fudian.file;


import com.acooly.module.openapi.client.file.impl.AbstractFileDataHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 测试的fudian 读取处理文件的 FileDataHandler
 * <p>
 * 该类实现：com.acooly.module.openapi.client.file.FileDataHandler 接口，通过name匹配自动从spring容器匹配获取。
 */
@Slf4j
@Component
public class FudianRechargeReadFileDataHandler extends AbstractFileDataHandler {


    @Override
    public void handle(String period, int index, List<String> rows, boolean finished) {
        log.info("page {}, period: {}, finished:{}", index, period, finished);
        for (String row : rows) {
            log.info(row);
        }
        log.info("");
    }

    @Override
    public int getBatchSize() {
        return 10;
    }

    @Override
    public String name() {
        return "recharge";
    }
}
