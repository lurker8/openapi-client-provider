package com.acooly.openapi.client.provider.wft;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.wft.WftApiService;
import com.acooly.module.openapi.client.provider.wft.message.*;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhike 2017/11/27 19:17
 */
@SpringBootApplication
@BootApp(sysName = "test")
public class WftTest extends NoWebTestBase {

    @Autowired
    private WftApiService wftApiService;


    /**
     * 测试微信支付宝被扫
     */
    @Test
    public void testWftUnifiedTradeMicropay() {
        //测试威富通被扫
        WftUnifiedTradeMicropayRequest request = new WftUnifiedTradeMicropayRequest();
        request.setAmount("1");
        request.setAuthCode("284427344486625084");
        request.setMchId("7551000001");
        request.setOutTradeNo(Ids.oid());
        request.setNonceStr(Ids.oid());
        request.setBody("等等");
        request.setDeviceInfo("4a:00:01:3d:f5:61");
        request.setGoodsTag("SHIPING1001");
        request.setMchCreateIp("192.168.45.234");
        WftUnifiedTradeMicropayResponse response = wftApiService.unifiedTradeMicropay(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试微信主扫
     */
    @Test
    public void testPayWeixinNative() {
        WftPayWeixinNativeRequest request = new WftPayWeixinNativeRequest();
        request.setAmount("1");
        request.setMchId("7551000001");
        request.setOutTradeNo(Ids.oid());
        request.setNonceStr(Ids.oid());
        request.setBody("等等");
        request.setDeviceInfo("4a:00:01:3d:f5:61");
        request.setGoodsTag("SHIPING1001");
        request.setMchCreateIp("192.168.45.234");
        WftPayWeixinNativeResponse response = wftApiService.payWeixinNative(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试支付宝主扫
     */
    @Test
    public void testPayAlipayNative() {
        WftPayAlipayNativeRequest request = new WftPayAlipayNativeRequest();
        request.setAmount("1");
        request.setOutTradeNo(Ids.oid());
        request.setNonceStr(Ids.oid());
        request.setBody("等等");
        request.setDeviceInfo("4a:00:01:3d:f5:61");
        request.setGoodsTag("SHIPING1001");
        request.setMchCreateIp("192.168.45.234");
        request.setQrCodeTimeoutExpress("2m");
        WftPayAlipayNativeResponse response = wftApiService.payAlipayNative(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试微信公众号支付
     */
    @Test
    public void testPayWeixinJspay() {
        WftPayWeixinJspayRequest request = new WftPayWeixinJspayRequest();
        request.setAmount("1");
        request.setIsRaw("1");
        request.setMchId("7551000001");
        request.setOutTradeNo(Ids.oid());
        request.setNonceStr(Ids.oid());
        request.setBody("等等");
        //测试帐号不用传appid和openId
//        request.setSubAppid("dsd");
//        request.setSubOpenid("ds");
        request.setDeviceInfo("4a:00:01:3d:f5:61");
        request.setGoodsTag("SHIPING1001");
        request.setMchCreateIp("192.168.45.234");
        WftPayWeixinJspayResponse response = wftApiService.payWeixinJspay(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试微信APP支付
     */
    @Test
    public void testPayWeixinRawApp() {
        WftPayWeixinRawAppRequest request = new WftPayWeixinRawAppRequest();
        request.setAmount("1");
        request.setMchId("755437000006");
        request.setOutTradeNo(Ids.oid());
        request.setNonceStr(Ids.oid());
        request.setBody("等等");
        request.setAppid("wx2a5538052969956e");
        request.setAppid("d232342342");
        request.setDeviceInfo("4a:00:01:3d:f5:61");
        request.setGoodsTag("SHIPING1001");
        request.setMchCreateIp("192.168.45.234");
        WftPayWeixinRawAppResponse response = wftApiService.payWeixinRawApp(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }


    /**
     * 测试订单查询接口
     */
    @Test
    public void testUnifiedTradeQuery() {
        WftUnifiedTradeQueryRequest request = new WftUnifiedTradeQueryRequest();
        request.setMchId("101520000465");
        request.setOutTradeNo("o19010913181961160002");
        request.setNonceStr(Ids.oid());
        WftUnifiedTradeQueryResponse response = wftApiService.unifiedTradeQuery(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试订单关闭接口
     */
    @Test
    public void testUnifiedTradeClose() {
        WftUnifiedTradeCloseRequest request = new WftUnifiedTradeCloseRequest();
        request.setMchId("101520000465");
        request.setOutTradeNo("o19010913181961160002");
        request.setNonceStr(Ids.oid());
        WftUnifiedTradeCloseResponse response = wftApiService.unifiedTradeClose(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试订单退款接口
     */
    @Test
    public void testUnifiedTradeRefund() {
        WftUnifiedTradeRefundRequest request = new WftUnifiedTradeRefundRequest();
        request.setMchId("403580137807");
        request.setOutTradeNo("o17120714141799100002");
        request.setTotalAmount("1");
        request.setRefundAmount("1");
        request.setOutRefundNo(Ids.oid());
        request.setOpUserId(Ids.oid());
        request.setNonceStr(Ids.oid());
        WftUnifiedTradeRefundResponse response = wftApiService.unifiedTradeRefund(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试订单退款查询接口
     */
    @Test
    public void testUnifiedTradeRefundQuery() {
        WftUnifiedTradeRefundQueryRequest request = new WftUnifiedTradeRefundQueryRequest();
        request.setMchId("7551000001");
        request.setOutTradeNo("o17120711164533400002");
        request.setOutRefundNo("o17120711261908150001");
        request.setNonceStr(Ids.oid());
        WftUnifiedTradeRefundQueryResponse response = wftApiService.unifiedTradeRefundQuery(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试订单撤销接口
     */
    @Test
    public void testUnifiedMicropayReverse() {
        WftUnifiedMicropayReverseRequest request = new WftUnifiedMicropayReverseRequest();
        request.setMchId("7551000001");
        request.setOutTradeNo("o17113010384307300002");
        request.setNonceStr(Ids.oid());
        WftUnifiedMicropayReverseResponse response = wftApiService.unifiedMicropayReverse(request);
        System.out.println("响应实体报文response=" + JSON.toJSONString(response));
    }

    /**
     * 测试下载对账文件接口
     */
    @Test
    public void testBillDownload() throws Exception{
        WftBillDownloadRequest request = new WftBillDownloadRequest();
        request.setBillDate("20171102");
        request.setNonceStr(Ids.oid());
        request.setFilePath("D:/var");
//        request.setFileName("bb");
//        request.setFileSuffix("doc");
        WftBillDownloadResponse response = wftApiService.billDownload(request);
        System.out.println("响应实体报文response="+JSON.toJSONString(response));
    }
}

