package com.acooly.openapi.client.provider.wsbank.merchantUnifiedRgister;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.wsbank.WsbankApiService;
import com.acooly.module.openapi.client.provider.wsbank.message.WsbankMerchantUnifiedRgisterRequest;
import com.acooly.module.openapi.client.provider.wsbank.message.WsbankMerchantUnifiedRgisterResponse;
import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankCommonMerchantClearCard;
import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankCommonMerchantDetail;
import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankCommonMerchantFeeRate;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankMerchantUnifiedRgisterRequestBody;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankMerchantUnifiedRgisterRequestInfo;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootApplication
@BootApp(sysName = "wsbankMerchantUnifiedRgisterTest")
public class WsbankMerchantUnifiedRgisterTest extends NoWebTestBase {
    @Autowired
    private WsbankApiService wsbankApiService;
    
    /**
     * 商户统一进件接口
     */
    @Test
    public void testUnifiedRegisteAccountType01() {
    	WsbankMerchantUnifiedRgisterRequestBody requestBody = new WsbankMerchantUnifiedRgisterRequestBody();
    	//赋值
    	requestBody.setOutMerchantId(Ids.oid());
    	requestBody.setMerchantName("汕头市悦涤日化有限公司");
    	requestBody.setMerchantType("02");
//    	requestBody.setAuthCode("251212");
    	requestBody.setOutTradeNo(Ids.oid());
    	//商户信息
    	WsbankCommonMerchantDetail wsbankCommonMerchantDetail = new WsbankCommonMerchantDetail();
    	wsbankCommonMerchantDetail.setAlias("汕头市悦涤日化有限公司");//商户简称
    	wsbankCommonMerchantDetail.setContactMobile("15651713678");//联系人手机号码
    	wsbankCommonMerchantDetail.setContactName("牛建跃");//联系人姓名
    	wsbankCommonMerchantDetail.setProvince("330000");//省
    	wsbankCommonMerchantDetail.setCity("330100");//市
    	wsbankCommonMerchantDetail.setDistrict("330102");//区
    	wsbankCommonMerchantDetail.setAddress("测试地址");//地址
    	wsbankCommonMerchantDetail.setServicePhoneNo("13182586606");//商户客服电话
    	wsbankCommonMerchantDetail.setPrincipalMobile("13182586606");//负责人手机号
    	wsbankCommonMerchantDetail.setPrincipalCertNo("132123197202150019");//负责人证件号码
    	wsbankCommonMerchantDetail.setPrincipalPerson("牛建跃");//负责人名称或企业法人代表姓名
    	wsbankCommonMerchantDetail.setBussAuthNum("914405073041792819");//商户号
    	wsbankCommonMerchantDetail.setCertPhotoA("53ff9889-42d4-4685-a8a4-4d50f0b5fd00");//负责人或企业法人代表的身份证图片正面
    	wsbankCommonMerchantDetail.setCertPhotoB("b28e401d-fef9-45f1-a142-58db8b0ddc83");//负责人或企业法人代表的身份证图片反面
    	wsbankCommonMerchantDetail.setLicensePhoto("c3c248c4-0a9a-411b-bd3a-f0c4e5a1aab5");//营业执照
    	requestBody.setWsbankCommonMerchantDetail(wsbankCommonMerchantDetail);
    	//微信站点信息
    	List<Map<String,String>> wsbankSiteInfoList = new ArrayList<Map<String,String>>();
    	Map<String,String> wsbankSiteInfo1 = new HashMap<String,String>();
    	wsbankSiteInfo1.put("SiteType", "01");
    	wsbankSiteInfo1.put("SiteUrl", "www");
    	wsbankSiteInfoList.add(wsbankSiteInfo1);
    	Map<String,String> wsbankSiteInfo2 = new HashMap<String,String>();
    	wsbankSiteInfo2.put("SiteType", "02");
    	wsbankSiteInfo2.put("SiteUrl", "www");
    	wsbankSiteInfoList.add(wsbankSiteInfo2);
    	Map<String,String> wsbankSiteInfo3 = new HashMap<String,String>();
    	wsbankSiteInfo3.put("SiteType", "03");
    	wsbankSiteInfo3.put("SiteUrl", "www");
    	wsbankSiteInfoList.add(wsbankSiteInfo3);
    	requestBody.setSiteInfo(JSON.toJSONString(wsbankSiteInfoList));
    	//手续费信息
    	List<WsbankCommonMerchantFeeRate> wsbankCommonMerchantFeeRateList = new ArrayList<WsbankCommonMerchantFeeRate>();
    	WsbankCommonMerchantFeeRate wsbankCommonMerchantFeeRate1 = new WsbankCommonMerchantFeeRate();
    	wsbankCommonMerchantFeeRate1.setChannelType("01");
    	wsbankCommonMerchantFeeRate1.setFeeType("02");
    	wsbankCommonMerchantFeeRate1.setFeeValue("0.0035");
    	wsbankCommonMerchantFeeRateList.add(wsbankCommonMerchantFeeRate1);
    	WsbankCommonMerchantFeeRate wsbankCommonMerchantFeeRate2 = new WsbankCommonMerchantFeeRate();
    	wsbankCommonMerchantFeeRate2.setChannelType("02");
    	wsbankCommonMerchantFeeRate2.setFeeType("02");
    	wsbankCommonMerchantFeeRate2.setFeeValue("0.0035");
    	wsbankCommonMerchantFeeRateList.add(wsbankCommonMerchantFeeRate2);
    	WsbankCommonMerchantFeeRate wsbankCommonMerchantFeeRate3 = new WsbankCommonMerchantFeeRate();
    	wsbankCommonMerchantFeeRate3.setChannelType("05");
    	wsbankCommonMerchantFeeRate3.setFeeType("02");
    	wsbankCommonMerchantFeeRate3.setFeeValue("0.0035");
    	wsbankCommonMerchantFeeRateList.add(wsbankCommonMerchantFeeRate3);
    	requestBody.setFeeParamList(JSON.toJSONString(wsbankCommonMerchantFeeRateList));
    	
    	//清算卡信息
    	WsbankCommonMerchantClearCard wsbankCommonMerchantClearCard = new WsbankCommonMerchantClearCard();
    	wsbankCommonMerchantClearCard.setBankCardNo("6212261202002415210");//银行卡号
    	wsbankCommonMerchantClearCard.setBankCertName("牛建跃");//银行账户户名
    	wsbankCommonMerchantClearCard.setAccountType("01");//账户类型
    	wsbankCommonMerchantClearCard.setCertType("01");
    	wsbankCommonMerchantClearCard.setCertNo("132123197202150019");
    	wsbankCommonMerchantClearCard.setCardHolderAddress("杭州");
    	
    	requestBody.setWsbankCommonMerchantClearCard(wsbankCommonMerchantClearCard);
        WsbankMerchantUnifiedRgisterRequestInfo requestInfo = new WsbankMerchantUnifiedRgisterRequestInfo();
        requestInfo.setRequestBody(requestBody);
        WsbankMerchantUnifiedRgisterRequest request = new WsbankMerchantUnifiedRgisterRequest();
        request.setRequestInfo(requestInfo);
        System.out.println("商户统一进件接口请求{}："+ JSON.toJSONString(request));
        WsbankMerchantUnifiedRgisterResponse response = wsbankApiService.merchantUnifiedRgister(request);
        System.out.println("商户统一进件接口："+ JSON.toJSONString(response));
    }
    
    
    /**
     * 商户统一进件接口
     */
    public void testUnifiedRegisteAccountType02() {
    	WsbankMerchantUnifiedRgisterRequestBody requestBody = new WsbankMerchantUnifiedRgisterRequestBody();
    	//赋值
    	requestBody.setOutMerchantId(Ids.oid());
    	requestBody.setMerchantName("汕头市悦涤日化有限公司");
    	requestBody.setMerchantType("03");
    	requestBody.setOutTradeNo(Ids.oid());
    	//商户信息
    	WsbankCommonMerchantDetail wsbankCommonMerchantDetail = new WsbankCommonMerchantDetail();
    	wsbankCommonMerchantDetail.setAlias("汕头市悦涤日化有限公司");//商户简称
    	wsbankCommonMerchantDetail.setContactMobile("15651713678");//联系人手机号码
    	wsbankCommonMerchantDetail.setContactName("牛建跃");//联系人姓名
    	wsbankCommonMerchantDetail.setProvince("330000");//省
    	wsbankCommonMerchantDetail.setCity("330100");//市
    	wsbankCommonMerchantDetail.setDistrict("330102");//区
    	wsbankCommonMerchantDetail.setAddress("测试地址");//地址
    	wsbankCommonMerchantDetail.setServicePhoneNo("13182586606");//商户客服电话
    	wsbankCommonMerchantDetail.setPrincipalMobile("13182586606");//负责人手机号
    	wsbankCommonMerchantDetail.setPrincipalCertNo("132123197202150019");//负责人证件号码
    	wsbankCommonMerchantDetail.setPrincipalPerson("牛建跃");//负责人名称或企业法人代表姓名
    	wsbankCommonMerchantDetail.setBussAuthNum("914405073041792819");//商户号
    	wsbankCommonMerchantDetail.setCertPhotoA("53ff9889-42d4-4685-a8a4-4d50f0b5fd00");//负责人或企业法人代表的身份证图片正面
    	wsbankCommonMerchantDetail.setCertPhotoB("b28e401d-fef9-45f1-a142-58db8b0ddc83");//负责人或企业法人代表的身份证图片反面
    	wsbankCommonMerchantDetail.setLicensePhoto("c3c248c4-0a9a-411b-bd3a-f0c4e5a1aab5");//营业执照
    	
    	wsbankCommonMerchantDetail.setLegalPerson("汕头市悦涤日化有限公司");//企业法人名称
    	wsbankCommonMerchantDetail.setIndustryLicensePhoto("0838576b-5adc-4fba-9258-c5dc536ef9af");//开户许可证照片
    	
    	requestBody.setWsbankCommonMerchantDetail(wsbankCommonMerchantDetail);
    	//微信站点信息
    	List<Map<String,String>> wsbankSiteInfoList = new ArrayList<Map<String,String>>();
    	Map<String,String> wsbankSiteInfo1 = new HashMap<String,String>();
    	wsbankSiteInfo1.put("SiteType", "01");
    	wsbankSiteInfo1.put("SiteUrl", "www");
    	wsbankSiteInfoList.add(wsbankSiteInfo1);
    	Map<String,String> wsbankSiteInfo2 = new HashMap<String,String>();
    	wsbankSiteInfo2.put("SiteType", "02");
    	wsbankSiteInfo2.put("SiteUrl", "www");
    	wsbankSiteInfoList.add(wsbankSiteInfo2);
    	Map<String,String> wsbankSiteInfo3 = new HashMap<String,String>();
    	wsbankSiteInfo3.put("SiteType", "03");
    	wsbankSiteInfo3.put("SiteUrl", "www");
    	wsbankSiteInfoList.add(wsbankSiteInfo3);
    	requestBody.setSiteInfo(JSON.toJSONString(wsbankSiteInfoList));
    	//手续费信息
    	List<WsbankCommonMerchantFeeRate> wsbankCommonMerchantFeeRateList = new ArrayList<WsbankCommonMerchantFeeRate>();
    	WsbankCommonMerchantFeeRate wsbankCommonMerchantFeeRate1 = new WsbankCommonMerchantFeeRate();
    	wsbankCommonMerchantFeeRate1.setChannelType("01");
    	wsbankCommonMerchantFeeRate1.setFeeType("02");
    	wsbankCommonMerchantFeeRate1.setFeeValue("0.0035");
    	wsbankCommonMerchantFeeRateList.add(wsbankCommonMerchantFeeRate1);
    	WsbankCommonMerchantFeeRate wsbankCommonMerchantFeeRate2 = new WsbankCommonMerchantFeeRate();
    	wsbankCommonMerchantFeeRate2.setChannelType("02");
    	wsbankCommonMerchantFeeRate2.setFeeType("02");
    	wsbankCommonMerchantFeeRate2.setFeeValue("0.0035");
    	wsbankCommonMerchantFeeRateList.add(wsbankCommonMerchantFeeRate2);
    	WsbankCommonMerchantFeeRate wsbankCommonMerchantFeeRate3 = new WsbankCommonMerchantFeeRate();
    	wsbankCommonMerchantFeeRate3.setChannelType("05");
    	wsbankCommonMerchantFeeRate3.setFeeType("02");
    	wsbankCommonMerchantFeeRate3.setFeeValue("0.0035");
    	wsbankCommonMerchantFeeRateList.add(wsbankCommonMerchantFeeRate3);
    	requestBody.setFeeParamList(JSON.toJSONString(wsbankCommonMerchantFeeRateList));
    	
    	//清算卡信息
    	WsbankCommonMerchantClearCard wsbankCommonMerchantClearCard = new WsbankCommonMerchantClearCard();
    	wsbankCommonMerchantClearCard.setBankCardNo("6212261202002415210");//银行卡号
    	wsbankCommonMerchantClearCard.setBankCertName("汕头市悦涤日化有限公司");//银行账户户名
    	wsbankCommonMerchantClearCard.setAccountType("02");//账户类型
    	wsbankCommonMerchantClearCard.setContactLine("314308900081");//联行号
    	
    	requestBody.setWsbankCommonMerchantClearCard(wsbankCommonMerchantClearCard);
        WsbankMerchantUnifiedRgisterRequestInfo requestInfo = new WsbankMerchantUnifiedRgisterRequestInfo();
        requestInfo.setRequestBody(requestBody);
        WsbankMerchantUnifiedRgisterRequest request = new WsbankMerchantUnifiedRgisterRequest();
        request.setRequestInfo(requestInfo);
        System.out.println("商户统一进件接口请求{}："+ JSON.toJSONString(request));
        WsbankMerchantUnifiedRgisterResponse response = wsbankApiService.merchantUnifiedRgister(request);
        System.out.println("商户统一进件接口："+ JSON.toJSONString(response));
    }
}
