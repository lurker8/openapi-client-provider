package com.acooly.module.openapi.client.provider.yipay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayApiMsg;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayResponse;
import com.acooly.module.openapi.client.provider.yipay.enums.YipayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/4/18 11:38
 */
@Getter
@Setter
@YipayApiMsg(service = YipayServiceNameEnum.REALTIME_LOCATION_VERIFY_POSITION,type = ApiMessageType.Response)
public class YipayRealTimeLocationVerifyPositionResponse extends YipayResponse {

    /**
     * 结果标识
     * 距离差编码：
     * 1-填写地址与实际地址距离小于等于3公里
     * 2-填写地址与实际地址距离大于3公里，小于等于10公里址
     * 3-填写地址与实际地址距离大于10公里，小于等于20公里
     * 4-填写地址与实际地址距离大于20公里，小于等于50公里
     * 5-填写地址与实际地址距离大于50公里
     */
    private String stat;

    /**
     * 结果描述
     */
    private String desc;

    /**
     * 定位时间
     * 格式：yyyy-MM-dd HH:mm:ss
     */
    private String locTime;
}
