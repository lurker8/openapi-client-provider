package com.acooly.module.openapi.client.provider.yipay;

import com.acooly.module.openapi.client.api.notify.ApiServiceClientServlet;
import com.acooly.module.openapi.client.api.transport.HttpTransport;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.yipay.file.YipaySftpFileHandler;
import com.acooly.module.openapi.client.provider.yipay.notify.YipayApiServiceClientServlet;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.util.List;


@EnableConfigurationProperties({OpenAPIClientYipayProperties.class})
@ConditionalOnProperty(value = OpenAPIClientYipayProperties.PREFIX + ".enable", matchIfMissing = true)
@ComponentScan
@ComponentScan("com.acooly.module.openapi.client.file")
public class OpenAPIClientYipayConfigration {

    @Autowired
    private OpenAPIClientYipayProperties openAPIClientYipayProperties;

    @Bean("yipayHttpTransport")
    public Transport yipayHttpTransport() {
        HttpTransport httpTransport = new HttpTransport();
        httpTransport.setGateway(openAPIClientYipayProperties.getGatewayUrl());
        httpTransport.setConnTimeout(String.valueOf(openAPIClientYipayProperties.getConnTimeout()));
        httpTransport.setReadTimeout(String.valueOf(openAPIClientYipayProperties.getReadTimeout()));
        httpTransport.setContentType("application/json");
        return httpTransport;
    }

    /**
     * 接受异步通知的filter
     *
     * @return
     */
    @Bean
    public ServletRegistrationBean yipayClientServlet() {
        ServletRegistrationBean bean = new ServletRegistrationBean();
        YipayApiServiceClientServlet apiServiceClientServlet = new YipayApiServiceClientServlet();
        bean.setServlet(apiServiceClientServlet);
        bean.addInitParameter(ApiServiceClientServlet.NOTIFY_DISPATCHER_BEAN_NAME_KEY, "yipayNotifyHandlerDispatcher");
        List<String> urlMappings = Lists.newArrayList();       //网关异步通知地址
        urlMappings.add(YipayConstants.getCanonicalUrl(openAPIClientYipayProperties.getNotifyUrl(), "/*"));//访问，可以添加多个
        bean.setUrlMappings(urlMappings);
        bean.setLoadOnStartup(3);
        return bean;
    }


    @Bean("yipaySftpFileHandler")
    public YipaySftpFileHandler yipaySftpFileHandler() {
        YipaySftpFileHandler yipaySftpFileHandler = new YipaySftpFileHandler();
        yipaySftpFileHandler.setHost(openAPIClientYipayProperties.getCheckfile().getHost());
        yipaySftpFileHandler.setPort(openAPIClientYipayProperties.getCheckfile().getPort());
        yipaySftpFileHandler.setUsername(openAPIClientYipayProperties.getCheckfile().getUsername());
        yipaySftpFileHandler.setPassword(openAPIClientYipayProperties.getCheckfile().getPassword());
        yipaySftpFileHandler.setServerRoot(openAPIClientYipayProperties.getCheckfile().getServerRoot());
        yipaySftpFileHandler.setLocalRoot(openAPIClientYipayProperties.getCheckfile().getLocalRoot());
        return yipaySftpFileHandler;
    }


}
