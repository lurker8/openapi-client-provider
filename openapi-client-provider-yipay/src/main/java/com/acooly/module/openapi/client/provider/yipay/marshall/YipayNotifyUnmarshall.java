/**
 * create by zhangpu
 * date:2015年3月12日
 */
package com.acooly.module.openapi.client.provider.yipay.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.provider.yipay.YipayConstants;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayNotify;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author zhangpu
 */
@Slf4j
@Component
public class YipayNotifyUnmarshall extends YipayAbstractMarshall implements ApiUnmarshal<YipayNotify, Map<String, String>> {
    
    @Override
    public YipayNotify unmarshal(Map<String, String> message, String serviceKey) {
        String crypt = message.get(YipayConstants.REQUEST_PARAM_NAME);
        return (YipayNotify) doUnMarshall(crypt, serviceKey, true);
    }
}
