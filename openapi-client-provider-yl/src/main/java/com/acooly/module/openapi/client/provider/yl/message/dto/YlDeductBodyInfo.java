package com.acooly.module.openapi.client.provider.yl.message.dto;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;

import org.hibernate.validator.constraints.Length;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * 签约订单参数
 * @author fufeng
 */
@Getter
@Setter
public class YlDeductBodyInfo implements Serializable {

    /**
     * 记录序号 (同一个请求内必须唯一。从0001开始递增)
     */
    @Length(min = 1, max = 25)
    private String bizOrderNo;

    /**
     * 商户订单号
     */
    private String merchOrderNo;

    /**
     * 真实姓名
     */
    private String payRealName;

    /**
     * 付款-证件类型
     */
    private String payCertType;

    /**
     * 身份证号
     */
    private String payCertNo;

    /**
     * 手机号
     */
    private String payMobileNo;

    /**
     * 付款-银行ID
     */
    private String payBankId;

    /**
     * 付款-银行名称
     */
    private String payBankName;

    /**
     * 付款-银行卡号
     */
    private String payBankCardNo;

    /**
     * 银行卡类型
     */
    private String payBankCardType;

    /**
     * 付款-对公:CORPORATE / 对私:PERSONAL 默认对私
     */
    protected String payAccountType;

    /**
     * 付款-银行省信息
     */
    private String payBankProvince;

    /**
     * 付款-银行市信息
     */
    private String payBankCity;

    /**
     * 金额
     */
    @MoneyConstraint(min = 1L)
    private Money amount;

    /**
     * 收款-卡号
     */
    private String recAccountNo;

    /**
     * 收款-卡号户名
     */
    private String recAccountName;

    /**
     * 收款-银行ID
     */
    private String recBankId;

    /**
     * 收款-银行名称
     */
    private String recBankName;

    /**
     * 收款-银行联行号
     */
    private String recBankLasalle;

    /**
     * 备注
     */
    private String memo;


    /** 银行通用名称 */
    private String bankGeneralName;

    /** 清算行号 */
    private String drctBankCode;

    /** 协议号 */
    private String protocolNo;

    /**
     * 交易明细号(查询用)
     */
    private String queryDetailSn;

    /**
     * 货币类型 ( 默认为 人民币：CNY 可空)
     */
    private String currency = "CNY";

    /**
     * 返回码
     */
    private String retCode;

    /**
     * 错误文本
     */
    private String errMsg;

}
