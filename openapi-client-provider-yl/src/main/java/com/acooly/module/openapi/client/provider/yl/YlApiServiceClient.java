/**
 * create by zhangpu
 * date:2015年3月2日
 */
package com.acooly.module.openapi.client.provider.yl;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.AbstractApiServiceClient;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.exception.ApiServerException;
import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.message.PostRedirect;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.yl.domain.YlNotify;
import com.acooly.module.openapi.client.provider.yl.domain.YlRequest;
import com.acooly.module.openapi.client.provider.yl.domain.YlResponse;
import com.acooly.module.openapi.client.provider.yl.enums.YlServiceEnum;
import com.acooly.module.openapi.client.provider.yl.marshall.YlNotifyUnmarshall;
import com.acooly.module.openapi.client.provider.yl.marshall.YlRedirectMarshall;
import com.acooly.module.openapi.client.provider.yl.marshall.YlRedirectPostMarshall;
import com.acooly.module.openapi.client.provider.yl.marshall.YlRequestMarshall;
import com.acooly.module.openapi.client.provider.yl.marshall.YlResponseUnmarshall;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

import javax.annotation.Resource;

import lombok.extern.slf4j.Slf4j;

/**
 * 上海银行P2P存管ApiService执行器  外部业务禁止使用
 *
 * @author zhangpu
 */
@Slf4j
@Component("ylApiServiceClient")
public class YlApiServiceClient
        extends AbstractApiServiceClient<YlRequest, YlResponse, YlNotify, YlNotify> {

    public static final String PROVIDER_NAME = "yl";

    @Resource(name = "ylHttpTransport")
    private Transport transport;
    @Resource(name = "ylRequestMarshall")
    private YlRequestMarshall requestMarshal;
    @Resource(name = "ylRedirectMarshall")
    private YlRedirectMarshall redirectMarshal;
    @Resource(name = "ylRedirectPostMarshall")
    private YlRedirectPostMarshall ylRedirectPostMarshall;

    @Autowired
    private YlResponseUnmarshall responseUnmarshal;

    @Autowired
    private YlNotifyUnmarshall notifyUnmarshal;

    @Autowired
    private YlProperties ylProperties;

    @Override
    public YlResponse execute(YlRequest request) {
        try {
            beforeExecute(request);
            if(Strings.isBlank(request.getGatewayUrl())) {
                request.setGatewayUrl(ylProperties.getGatewayUrl());
            }
            String url = request.getGatewayUrl();
            String requestMessage = getRequestMarshal().marshal(request);
            log.info("请求报文: {}", requestMessage);
            String responseMessage = "";
            if(Strings.equals(YlServiceEnum.YL_BILL_DOWNLOAD.getCode(),request.getTrxCode())){
                responseMessage = getTransport().exchange(requestMessage, requestMessage);
            }else{
                responseMessage = getTransport().exchange(requestMessage, url);
            }
            YlResponse t = getResponseUnmarshal().unmarshal(responseMessage, request.getService());
            afterExecute(t);
            return t;
        } catch (ApiServerException ose) {
            log.error("服务器:" + ose.getMessage(), ose);
            throw ose;
        } catch (ApiClientException oce) {
            log.error("客户端:" + oce.getMessage(), oce);
            throw oce;
        } catch (Exception e) {
            log.error("内部错误:" + e.getMessage(), e);
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }

    @Override
    public PostRedirect redirectPost(YlRequest request) {
        try {
            beforeExecute(request);
            PostRedirect postRedirect = ylRedirectPostMarshall.marshal(request);
            return postRedirect;
        } catch (ApiServerException ose) {
            log.error("服务器:" + ose.getMessage(), ose);
            throw ose;
        } catch (ApiClientException oce) {
            log.error("客户端:" + oce.getMessage(), oce);
            throw oce;
        } catch (Exception e) {
            log.error("内部错误:" + e.getMessage(), e);
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }

    @Override
    protected ApiMarshal<String, YlRequest> getRequestMarshal() {
        return this.requestMarshal;
    }

    @Override
    protected ApiUnmarshal<YlResponse, String> getResponseUnmarshal() {
        return this.responseUnmarshal;
    }

    @Override
    protected ApiUnmarshal<YlNotify, Map<String, String>> getNoticeUnmarshal() {
        return this.notifyUnmarshal;
    }

    @Override
    protected ApiMarshal<String, YlRequest> getRedirectMarshal() {
        return this.redirectMarshal;
    }

    @Override
    protected Transport getTransport() {
        return this.transport;
    }

    @Override
    protected ApiUnmarshal<YlNotify, Map<String, String>> getReturnUnmarshal() {
        return this.notifyUnmarshal;
    }

    @Override
    public String getName() {
        return PROVIDER_NAME;
    }


}
