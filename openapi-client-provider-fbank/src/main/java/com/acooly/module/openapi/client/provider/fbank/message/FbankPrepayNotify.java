package com.acooly.module.openapi.client.provider.fbank.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankNotify;
import com.acooly.module.openapi.client.provider.fbank.enums.FbankServiceEnum;
import com.acooly.module.openapi.client.provider.fbank.enums.FbankServiceTypeEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike@qq.com
 * @date 2018-09-06 16:08
 */
@Getter
@Setter
@FbankApiMsgInfo(service = FbankServiceEnum.PREPAY_API, type = ApiMessageType.Notify,serviceType = FbankServiceTypeEnum.payol)
public class FbankPrepayNotify extends FbankNotify {

    /**
     * 订单总金额
     * 必须为正整数（单位为分）
     */
    @NotBlank
    @Size(max = 12)
    private String amount;

    /**
     * 商品名称
     * 商户自定义订单标题
     */
    @NotBlank
    @Size(max = 64)
    private String subject;

    /**
     * 订单描述
     * 商户自定义订单描述
     */
    @NotBlank
    @Size(max = 256)
    private String body;

    /**
     * 商户订单号
     * 商户自己平台的订单号（唯一）
     */
    @NotBlank
    @Size(max = 64)
    private String mchntOrderNo;

    /**
     * 支付功能id
     */
    @NotBlank
    @Size(max = 12)
    private String payPowerId;

    /**
     * 平台订单号
     * 支付平台生成的订单号（唯一）
     */
    @NotBlank
    @Size(max = 32)
    private String orderNo;

    /**
     * 支付结果
     * 条码支付成功时有返回(支付宝、微信)
     * 0:待支付;1:支付中(已请求上游);2:支付成功;3:支付失败;4:退款;5:已关闭;6:撤销;
     */
    @NotBlank
    @Size(max = 1)
    private String paySt;

    /**
     * 第三方流水号
     * 第三方流水号(支付宝、微信)
     */
    @Size(max = 64)
    private String bankTransactionId;

    /**
     * 下单日期
     * 订单创建时间；格式：yyyyMMdd
     */
    @Size(max = 8)
    private String orderDt;

    /**
     * 支付完成时间
     * 格式：yyyy-MM-dd HH:mm:ss
     */
    @Size(max = 30)
    private String paidTime;

    /**
     * 订单附加描述
     * 商户下单时所传
     */
    @Size(max = 512)
    private String description;

    /**
     * 订单手续费
     * 该笔订单所扣费用（单位为分）
     */
    @Size(max = 12)
    @NotBlank
    private String fee;

    /**
     * 附加数据
     * 商户下单时所传
     */
    @Size(max = 256)
    private String extra;

    /**
     * 支付用户信息
     * 该笔订单的支付用户信息
     */
    @Size(max = 64)
    private String openId;

}
