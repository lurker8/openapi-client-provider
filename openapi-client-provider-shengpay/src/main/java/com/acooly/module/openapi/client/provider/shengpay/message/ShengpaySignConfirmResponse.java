package com.acooly.module.openapi.client.provider.shengpay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayApiMsg;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayResponse;
import com.acooly.module.openapi.client.provider.shengpay.enums.ShengpayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author zhike 2018/5/17 18:56
 */
@Getter
@Setter
@ShengpayApiMsg(service = ShengpayServiceNameEnum.SIGN_CONFIRM,type = ApiMessageType.Response)
public class ShengpaySignConfirmResponse extends ShengpayResponse {

    /**
     * 签约协议号
     */
    @NotBlank
    private String agreementNo;
}
