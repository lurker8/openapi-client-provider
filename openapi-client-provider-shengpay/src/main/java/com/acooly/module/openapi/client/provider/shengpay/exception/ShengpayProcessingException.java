package com.acooly.module.openapi.client.provider.shengpay.exception;

/**
 * @author zhike 2018/3/15 14:28
 */
public class ShengpayProcessingException extends RuntimeException{

    public ShengpayProcessingException() {
    }

    public ShengpayProcessingException(String message, Throwable cause) {
        super(message, cause);
    }

    public ShengpayProcessingException(String message) {
        super(message);
    }

    public ShengpayProcessingException(Throwable cause) {
        super(cause);
    }
}
