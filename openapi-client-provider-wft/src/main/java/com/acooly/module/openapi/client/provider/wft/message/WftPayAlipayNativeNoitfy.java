package com.acooly.module.openapi.client.provider.wft.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wft.domain.WftApiMsgInfo;
import com.acooly.module.openapi.client.provider.wft.domain.WftNotify;
import com.acooly.module.openapi.client.provider.wft.enums.WftServiceEnum;
import com.acooly.module.openapi.client.provider.wft.support.WftAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2017/11/28 21:02
 */
@Getter
@Setter
@WftApiMsgInfo(service = WftServiceEnum.PAY_ALIPAY_NATIVE, type = ApiMessageType.Notify)
public class WftPayAlipayNativeNoitfy extends WftNotify{

    /**
     * --------------------------以下字段在 status 和 result_code 都为 0的时候有返回--------------------------------
     */

    /**
     * 用户标识 用户在受理商户 appid 下的唯一标识
     */
    @WftAlias("openid")
    private String openId;

    /**
     * pay.weixin.micropay——微信刷卡支付 pay.alipay.micropay——支付宝刷卡支付 pay.jdpay.micropay——京东刷卡支付
     * pay.qq.micropay——QQ钱包刷卡支付 pay.shiming.micropay——会员卡支付 pay.unionpay.micropay——银联支付
     * pay.bestpay.micropay——翼支付
     */
    @WftAlias("trade_type")
    private String tradeType;

    /**
     * 是否关注公众账号 用户是否关注公众账号，Y-关注，N-未关注，仅在公众账号类型支付有效
     */
    @WftAlias("is_subscribe")
    private String isSubscribe;

    /**
     * 支付结果 支付结果：0—成功；其它—失败
     */
    @WftAlias("pay_result")
    private String payResult;

    /**
     * 支付结果信息 ，支付成功时为空
     */
    @WftAlias("pay_info")
    private String payInfo;

    /**
     * 平台订单号
     */
    @WftAlias("transaction_id")
    private String transactionId;

    /**
     * 第三方订单号
     */
    @WftAlias("out_transaction_id")
    private String outTransactionId;

    /**
     * 子商户是否关注 用户是否关注子公众账号，0-关注，1-未关注，仅在公众账号类型支付有效
     */
    @WftAlias("sub_is_subscribe")
    private String subIsSubscribe;

    /**
     * 子商户appid
     */
    @WftAlias("sub_appid")
    private String subAppid;

    /**
     * 用户在商户公众号appid 下的唯一标识
     */
    @WftAlias("sub_openid")
    private String subOpenid;

    /**
     * 订单号
     */
    @WftAlias("out_trade_no")
    private String outTradeNo;

    /**
     * 总金额
     */
    @WftAlias("total_fee")
    private String amount;

    /**
     * 现金券金额【微信】现金券支付金额<=订单总金额， 订单总金额-现金券金额为现金支付金额
     */
    @WftAlias("coupon_fee")
    private String couponFee;

    /**
     * 货币类型，符合 ISO 4217 标准的三位字母代码，默认人民币：CNY
     */
    @WftAlias("fee_type")
    private String feeType;

    /**
     * 附加信息 商家数据包，原样返回
     */
    @WftAlias("attach")
    private String attach;

    /**
     * 银行类型
     */
    @WftAlias("bank_type")
    private String bankType;

    /**
     * 银行订单号，若为微信支付则为空
     */
    @WftAlias("bank_billno")
    private String bankBillno;

    /**
     * 支付完成时间，格式为yyyyMMddhhmmss，如2009年12月27日9点10分10秒表示为20091227091010。时区为GMT+8 beijing。该时间取自平台服务器
     */
    @WftAlias("time_end")
    private String timeEnd;
}
