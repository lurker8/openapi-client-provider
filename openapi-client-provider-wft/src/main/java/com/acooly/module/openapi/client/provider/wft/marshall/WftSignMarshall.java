package com.acooly.module.openapi.client.provider.wft.marshall;

import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.provider.wft.WftConstants;
import com.acooly.module.openapi.client.provider.wft.utils.SignUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * @author weichk
 */
@Slf4j
@Service
public class WftSignMarshall extends WftMarshallSupport {

    /**
     * 签名
     *
     * @return
     */
    public String sign(Map<String, String> waitSignMap,String partnerId) {
        SortedMap<String,String> sort=new TreeMap<String,String>(waitSignMap);
        String signature = doSign(sort,partnerId);
        return signature;
    }

    /**
     * 验签
     * @param request
     * @return
     */
    public boolean verySign(HttpServletRequest request,String partnerId) {
        boolean isPass = true;
        try {
            Map<String, String> waitSignMap = getDateMap(request);
            String signature = waitSignMap.get(WftConstants.SIGN);
            SortedMap<String,String> sort=new TreeMap<String,String>(waitSignMap);
            doVerifySign(sort, signature,partnerId);
        } catch (Exception e) {
            isPass = false;
            log.info("验签失败[" + e.getMessage() + "]");
        }
        return isPass;
    }

    /**
     * 验签
     * @param waitSignMap
     * @return
     */
    public boolean verySign(Map<String, String> waitSignMap,String partnerId) {
        boolean isPass = true;
        try {
            String signature = waitSignMap.get(WftConstants.SIGN);
            SortedMap<String,String> sort=new TreeMap<String,String>(waitSignMap);
            doVerifySign(sort, signature,partnerId);
        } catch (Exception e) {
            isPass = false;
            log.info("验签失败[" + e.getMessage() + "]");
        }
        return isPass;
    }

    /**
     * 将收到的报文转化为map
     *
     * @param request
     * @return
     */
    public Map<String, String> getDateMap(HttpServletRequest request) {
        try {
            request.setCharacterEncoding("utf-8");
            Map<String, String> params = new HashMap<String, String>();
            Map<String, String[]> requestParams = request.getParameterMap();
            for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
                String name = entry.getKey();
                String[] values = entry.getValue();
                String valueStr = "";
                if (values != null) {
                    for (int i = 0; i < values.length; i++) {
                        valueStr = (i == values.length - 1) ? valueStr + values[i]
                                : valueStr + values[i] + ",";
                    }

                    params.put(name, valueStr);
                }

            }
            return params;
        } catch (Exception e) {
            throw new ApiClientException("签名失败", e);
        }
    }
}
