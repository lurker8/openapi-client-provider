package com.acooly.module.openapi.client.provider.wft.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wft.domain.WftApiMsgInfo;
import com.acooly.module.openapi.client.provider.wft.domain.WftResponse;
import com.acooly.module.openapi.client.provider.wft.enums.WftServiceEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2017/11/28 20:41
 */
@Getter
@Setter
@WftApiMsgInfo(service = WftServiceEnum.UNIFIED_TRADE_CLOSE, type = ApiMessageType.Response)
public class WftUnifiedTradeCloseResponse extends WftResponse{
}
