package com.acooly.module.openapi.client.provider.webank.message;


import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.webank.domain.WeBankApiMsgInfo;
import com.acooly.module.openapi.client.provider.webank.domain.WeBankResponse;
import com.acooly.module.openapi.client.provider.webank.enums.WeBankServiceEnum;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@WeBankApiMsgInfo(service = WeBankServiceEnum.WEBANK_WITHDRAW_QUERY, type = ApiMessageType.Response)
public class WeBankWithdrawQueryResponse extends WeBankResponse {

    /** 响应码 */
    String respCode;
    /** 响应码描述 */
    String respMsg;
    /** 商户号 */
    String merId;
    /** 订单号 */
    String orderId;

    /** 交易类型 */
    String bizType;

    /** 签名 */
    String signature;

    /** 交易状态 */
    String status;

    /** 交易金额 */
    String amount;

    /** 原交易类型 */
    String oriBizType;

    /** 清算日期 */
    String settleDate;

    /** 余额 */
    private String accBalance;

}
