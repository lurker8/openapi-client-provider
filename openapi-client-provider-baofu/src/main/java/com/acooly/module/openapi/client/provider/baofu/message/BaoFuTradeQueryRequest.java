package com.acooly.module.openapi.client.provider.baofu.message;

import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuRequest;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum;
import com.acooly.module.openapi.client.provider.baofu.support.BaoFuAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/1/29 11:37
 */
@Getter
@Setter
@BaoFuApiMsgInfo(service = BaoFuServiceEnum.TRADE_QUERY,type = ApiMessageType.Request)
public class BaoFuTradeQueryRequest extends BaoFuRequest{

    /**
     * 接入类型 默认0000,表示为储蓄卡支付。
     */
    @NotBlank
    @BaoFuAlias(value = "biz_type", request = false)
    private String bizType;


    /**
     * 原始商户订单号
     * 商户提交的标识支付的唯一原订单号
     */
    @NotBlank
    @BaoFuAlias(value = "orig_trans_id", request = false)
    private String origTransId;

    /**
     * 订单日期 14 位定长。
     * 格式：年年年年月月日日时时分分秒秒
     */
    @NotBlank
    @BaoFuAlias(value = "orig_trade_date", request = false)
    private String origTradeDate;

    /**
     * 商户流水号 8-20 位字母和数字，每次请求都不可重复
     *
     */
    @BaoFuAlias(value = "trans_serial_no",request = false)
    private String transSerialNo = Ids.Did.getInstance().getId(20);

    /**
     * 请求方保留域
     */
    @BaoFuAlias(value = "req_reserved")
    private String reqReserved;

    /**
     * 附加字段
     */
    @BaoFuAlias(value = "additional_info")
    @Size(max = 128)
    private String additionalInfo;
}
