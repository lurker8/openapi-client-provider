/**
 * create by zhike date:2015年3月12日
 */
package com.acooly.module.openapi.client.provider.baofu.marshall;

import com.acooly.core.utils.Reflections;
import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.message.MessageFactory;
import com.acooly.module.openapi.client.provider.baofu.BaoFuConstants;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuNotify;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuResponse;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceIsZIPEnum;
import com.acooly.module.openapi.client.provider.baofu.marshall.info.ResponseInfo;
import com.acooly.module.openapi.client.provider.baofu.support.BaoFuAlias;
import com.acooly.module.openapi.client.provider.baofu.utils.BaoFuSecurityUtil;
import com.acooly.module.openapi.client.provider.baofu.utils.XmlUtils;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * @author zhike
 */
@Service
@Slf4j
public class BaoFuNotifyUnmarshall extends BaoFuMarshallSupport
        implements ApiUnmarshal<BaoFuNotify, Map<String, String>> {
    private static final Logger logger = LoggerFactory.getLogger(BaoFuNotifyUnmarshall.class);

    @Resource(name = "baoFuMessageFactory")
    private MessageFactory messageFactory;

    @SuppressWarnings("unchecked")
    @Override
    public BaoFuNotify unmarshal(Map<String, String> notifyMessage, String serviceName) {
        try {
            log.info("异步通知报文{}", notifyMessage);
            //解密
            String notifyEncodeContent = notifyMessage.get(BaoFuConstants.BAOFU_DATA_CONTENT);
            String partnerId = notifyMessage.get(BaoFuConstants.PARTNERID_KEY);
            boolean isZip = false;
            try {
                BaoFuServiceIsZIPEnum baoFuServiceIsZIP = BaoFuServiceIsZIPEnum.find(serviceName);
                if(baoFuServiceIsZIP != null) {
                    isZip = true;
                }
                //解密报文
                String decodeDataContent = doDecodeResponse(notifyEncodeContent,partnerId,isZip);
                if(!isZip) {
                    decodeDataContent = BaoFuSecurityUtil.Base64Decode(decodeDataContent);
                }
                return doUnmarshall(decodeDataContent, serviceName);
            }catch (Exception e) {
                log.info("Base64代签字符串失败：{}",e.getMessage());
                throw new ApiClientException(e.getMessage());
            }
        } catch (Exception e) {
            throw new ApiClientException("异步通知 解析失败:" + e.getMessage());
        }
    }

    protected BaoFuNotify doUnmarshall(String resMessage, String serviceName) {
        BaoFuNotify notify =
                (BaoFuNotify) messageFactory.getNotify(BaoFuServiceEnum.find(serviceName).getKey());
        notify = XmlUtils.toBean(resMessage,notify.getClass());
        return notify;
    }

    public void setMessageFactory(MessageFactory messageFactory) {
        this.messageFactory = messageFactory;
    }
}
