/**
 * create by zhike
 * date:2015年3月20日
 */
package com.acooly.module.openapi.client.provider.baofu;

import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuNotify;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceVersionEnum;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuTxnTypeEnum;
import com.acooly.module.openapi.client.provider.baofu.message.*;
import com.acooly.module.openapi.client.provider.baofu.message.info.BaoFuAccountBalanceQueryTransReqData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author zhike
 */
@Service
public class BaoFuApiService {

    @Resource(name = "baofuApiServiceClient")
    private BaoFuApiServiceClient apiServiceClient;

    @Autowired
    private OpenAPIClientBaoFuProperties openAPIClientBaoFuProperties;

    private static final String BIZ_TYPE = "0000";

    /**
     * 代扣
     * @param request
     * @return
     */
    public BaoFuDeductResponse deduct(BaoFuDeductRequest request) {
        request.setService(BaoFuServiceEnum.DEDUCT.getCode());
        request.setTxnSubType(BaoFuServiceEnum.DEDUCT.getKey());
        request.setVersion(BaoFuServiceVersionEnum.VERSION_4000.getCode());
        request.setTxnType(BaoFuTxnTypeEnum.TXN_TYPE_0431.getCode());
        request.setBizType(BIZ_TYPE);
        return (BaoFuDeductResponse)apiServiceClient.execute(request);
    }

    /**
     * 单笔代扣订单查询
     * @param request
     * @return
     */
    public BaoFuTradeQueryResponse tradeQuery(BaoFuTradeQueryRequest request) {
        request.setService(BaoFuServiceEnum.TRADE_QUERY.getCode());
        request.setTxnSubType(BaoFuServiceEnum.TRADE_QUERY.getKey());
        request.setVersion(BaoFuServiceVersionEnum.VERSION_4000.getCode());
        request.setTxnType(BaoFuTxnTypeEnum.TXN_TYPE_0431.getCode());
        request.setBizType(BIZ_TYPE);
        return (BaoFuTradeQueryResponse)apiServiceClient.execute(request);
    }

    /**
     * 批量代扣申请
     * @param request
     * @return
     */
    public BaoFuBatchDeductResponse batchDeduct(BaoFuBatchDeductRequest request) {
        request.setService(BaoFuServiceEnum.BATCH_DEDUCT.getCode());
        request.setTxnSubType(BaoFuServiceEnum.BATCH_DEDUCT.getKey());
        request.setVersion(BaoFuServiceVersionEnum.VERSION_4002.getCode());
        request.setTxnType(BaoFuTxnTypeEnum.TXN_TYPE_0631.getCode());
        return (BaoFuBatchDeductResponse)apiServiceClient.execute(request);
    }

    /**
     * 批量代扣单笔子订单查询
     * @param request
     * @return
     */
    public BaoFuBatchDeductSigleQueryResponse batchDeductSigleQuery(BaoFuBatchDeductSigleQueryRequest request) {
        request.setService(BaoFuServiceEnum.BATCH_DEDUCT_SIGLE_QUERY.getCode());
        request.setTxnSubType(BaoFuServiceEnum.BATCH_DEDUCT_SIGLE_QUERY.getKey());
        request.setVersion(BaoFuServiceVersionEnum.VERSION_4002.getCode());
        request.setTxnType(BaoFuTxnTypeEnum.TXN_TYPE_0631.getCode());
        return (BaoFuBatchDeductSigleQueryResponse)apiServiceClient.execute(request);
    }

    /**
     * 批量提现接口
     * @param request
     * @return
     */
    public BaoFuWithdrawResponse withdraw(BaoFuWithdrawRequest request) {
        request.setService(BaoFuServiceEnum.WITHDRAW.getCode());
        request.setVersion(BaoFuServiceVersionEnum.VERSION_400.getCode());
        return (BaoFuWithdrawResponse)apiServiceClient.withdrawExecute(request);
    }

    /**
     * 批量提现查询接口
     * @param request
     * @return
     */
    public BaoFuWithdrawQueryResponse withdrawQuery(BaoFuWithdrawQueryRequest request) {
        request.setService(BaoFuServiceEnum.WITHDRAW_QUERY.getCode());
        request.setVersion(BaoFuServiceVersionEnum.VERSION_400.getCode());
        return (BaoFuWithdrawQueryResponse)apiServiceClient.withdrawExecute(request);
    }

    /**
     * 退款API
     * @param request
     * @return
     */
    public BaoFuRefundResponse refund(BaoFuRefundRequest request) {
        request.setService(BaoFuServiceEnum.REFUND.getCode());
        request.setTxnSubType(BaoFuServiceEnum.REFUND.getKey());
        request.setVersion(BaoFuServiceVersionEnum.VERSION_4000.getCode());
        request.setTxnType(BaoFuTxnTypeEnum.TXN_TYPE_331.getCode());
        request.setNoticeUrl(openAPIClientBaoFuProperties.getDomainUrl());
        return (BaoFuRefundResponse)apiServiceClient.execute(request);
    }

    /**
     * 退款查询接口
     * @param request
     * @return
     */
    public BaoFuRefundQueryResponse refundQuery(BaoFuRefundQueryRequest request) {
        request.setService(BaoFuServiceEnum.REFUND_QUERY.getCode());
        request.setVersion(BaoFuServiceVersionEnum.VERSION_4000.getCode());
        request.setTxnSubType(BaoFuServiceEnum.REFUND_QUERY.getKey());
        request.setTxnType(BaoFuTxnTypeEnum.TXN_TYPE_331.getCode());
        return (BaoFuRefundQueryResponse)apiServiceClient.execute(request);
    }

    /**
     * 账户余额查询
     * @param request
     * @return
     */
    public BaoFuAccountBalanceQueryResponse accountBalanceQuery(BaoFuAccountBalanceQueryRequest request) {
        request.setService(BaoFuServiceEnum.ACCOUNT_BALANCE_QUERY.getCode());
        request.setVersion(BaoFuServiceVersionEnum.VERSION_400.getCode());
        return (BaoFuAccountBalanceQueryResponse)apiServiceClient.accountBalanceQuery(request);
    }
    /**
     * 对账文件下载
     *
     * @param request
     * @return
     */
    public BaoFuBillDowloadResponse billDownload(BaoFuBillDowloadRequest request) {
        request.setVersion(BaoFuServiceVersionEnum.VERSION_4002.getCode());
        return apiServiceClient.billDownload(request);
    }

    /**
     * 解析异步通知
     *
     * @param request
     * @param serviceKey
     * @return
     */
    public BaoFuNotify notice(HttpServletRequest request, String serviceKey) {
        return apiServiceClient.notice(request, serviceKey);
    }
}
