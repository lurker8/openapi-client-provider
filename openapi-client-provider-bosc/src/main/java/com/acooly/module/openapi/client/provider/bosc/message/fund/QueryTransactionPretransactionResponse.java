package com.acooly.module.openapi.client.provider.bosc.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponse;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.message.fund.info.PreTransactionDetailInfo;

import java.util.List;

@ApiMsgInfo(service = BoscServiceNameEnum.QUERY_TRANSACTION_PRETRANSACTION ,type = ApiMessageType.Response)
public class QueryTransactionPretransactionResponse extends BoscResponse {
	
	private List<PreTransactionDetailInfo> records;
	
	
	public List<PreTransactionDetailInfo> getRecords () {
		return records;
	}
	
	public void setRecords (
			List<PreTransactionDetailInfo> records) {
		this.records = records;
	}
}