/*
 *
 * www.cutebear.com Inc.
 * Copyright (c) 2017  All Rights Reserved
 */

package com.acooly.module.openapi.client.provider.bosc.enums.fund;

import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 充值状态
 * Created by liubin@prosysoft.com on 2017/9/27.
 */
public enum BoscRechargeStatusEnum {
	
	SUCCESS ("SUCCESS", "成功"),
	FAIL ("FAIL", "失败"),
	PENDDING ("PENDDING", "支付中"),
	;
	
	
	private final String code;
	private final String message;
	
	private BoscRechargeStatusEnum (String code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public String getCode () {
		return code;
	}
	
	public String getMessage () {
		return message;
	}
	
	public String code () {
		return code;
	}
	
	public String message () {
		return message;
	}
	
	public static Map<String, String> mapping () {
		Map<String, String> map = Maps.newLinkedHashMap ();
		for (BoscRechargeStatusEnum type : values ()) {
			map.put (type.getCode (), type.getMessage ());
		}
		return map;
	}
	
	/**
	 * 通过枚举值码查找枚举值。
	 *
	 * @param code 查找枚举值的枚举值码。
	 *
	 * @return 枚举值码对应的枚举值。
	 * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
	 */
	public static BoscRechargeStatusEnum find (String code) {
		for (BoscRechargeStatusEnum status : values ()) {
			if (status.getCode ().equals (code)) {
				return status;
			}
		}
		throw new IllegalArgumentException ("BoscAuditStatusEnum not legal:" + code);
	}
	
	/**
	 * 获取全部枚举值。
	 *
	 * @return 全部枚举值。
	 */
	public static List<BoscRechargeStatusEnum> getAll () {
		List<BoscRechargeStatusEnum> list = new ArrayList<BoscRechargeStatusEnum> ();
		for (BoscRechargeStatusEnum status : values ()) {
			list.add (status);
		}
		return list;
	}
	
	/**
	 * 获取全部枚举值码。
	 *
	 * @return 全部枚举值码。
	 */
	public static List<String> getAllCode () {
		List<String> list = new ArrayList<String> ();
		for (BoscRechargeStatusEnum status : values ()) {
			list.add (status.code ());
		}
		return list;
	}
	
}
