/*
 *
 * www.cutebear.com Inc.
 * Copyright (c) 2017  All Rights Reserved
 */

package com.acooly.module.openapi.client.provider.bosc.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscTransactionTypeEnum;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * Created by liubin@prosysoft.com on 2017/10/16.
 */
@ApiMsgInfo(service = BoscServiceNameEnum.QUERY_TRANSACTION,type = ApiMessageType.Request)
public class QueryTransactionRequest extends BoscRequest {

	@NotBlank(message = "请求流水号不能为空")
	private String requestNo;
	
	@NotNull(message = "查询交易类型不能为空")
	private BoscTransactionTypeEnum transactionType;
	
	
	public QueryTransactionRequest () {
		setService (BoscServiceNameEnum.QUERY_TRANSACTION.code ());
	}
	
	public QueryTransactionRequest (String requestNo,
	                                BoscTransactionTypeEnum transactionType) {
		this();
		this.requestNo = requestNo;
		this.transactionType = transactionType;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public BoscTransactionTypeEnum getTransactionType () {
		return transactionType;
	}
	
	public void setTransactionType (BoscTransactionTypeEnum transactionType) {
		this.transactionType = transactionType;
	}
}
