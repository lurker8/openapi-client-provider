/*
 *
 * www.cutebear.com Inc.
 * Copyright (c) 2017  All Rights Reserved
 */

package com.acooly.module.openapi.client.provider.bosc.message.fund.info;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by liubin@prosysoft.com on 2017/10/16.
 */
public class DebentureSaleDetailInfo {
	
	/**
	 * 债权出让平台用户编号
	 */
	@NotEmpty
	@Size(max = 50)
	private String platformUserNo;
	/**
	 * 标的号
	 */
	@NotEmpty
	@Size(max = 50)
	private String projectNo;
	/**
	 * 出让份额
	 */
	@MoneyConstraint
	private Money saleShare;
	/**
	 * 累计预处理中份额
	 */
	@MoneyConstraint
	private Money preSalingShare;
	/**
	 * 累计已确认份额
	 */
	@MoneyConstraint
	private Money confirmedShare;
	/**
	 * 债权出让订单状态，ONSALE 表示出让中，COMPLETED 表示已结束,
	 */
	@NotEmpty
	@Size(max = 50)
	private String status;
	/**
	 * 交易发起时间
	 */
	@NotNull
	private Date createTime;
	/**
	 * 交易完成时间，预处理中份额全部解冻的时间
	 */
	private Date transactionTime;
	
	public String getPlatformUserNo () {
		return platformUserNo;
	}
	
	public void setPlatformUserNo (String platformUserNo) {
		this.platformUserNo = platformUserNo;
	}
	
	public String getProjectNo () {
		return projectNo;
	}
	
	public void setProjectNo (String projectNo) {
		this.projectNo = projectNo;
	}
	
	public Money getSaleShare () {
		return saleShare;
	}
	
	public void setSaleShare (Money saleShare) {
		this.saleShare = saleShare;
	}
	
	public Money getPreSalingShare () {
		return preSalingShare;
	}
	
	public void setPreSalingShare (Money preSalingShare) {
		this.preSalingShare = preSalingShare;
	}
	
	public Money getConfirmedShare () {
		return confirmedShare;
	}
	
	public void setConfirmedShare (Money confirmedShare) {
		this.confirmedShare = confirmedShare;
	}
	
	public String getStatus () {
		return status;
	}
	
	public void setStatus (String status) {
		this.status = status;
	}
	
	public Date getCreateTime () {
		return createTime;
	}
	
	public void setCreateTime (Date createTime) {
		this.createTime = createTime;
	}
	
	public Date getTransactionTime () {
		return transactionTime;
	}
	
	public void setTransactionTime (Date transactionTime) {
		this.transactionTime = transactionTime;
	}
}
