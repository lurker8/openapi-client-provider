package com.acooly.module.openapi.client.provider.hx.message.xStream.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import org.hibernate.validator.constraints.NotBlank;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Data;

/**
 * @author fufeng 2018/1/26 15:26.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("head")
public class ReqHead {

    /**
     * 版本号
     */
    @XStreamAlias("Version")
    @NotBlank
    private String version;

    /**
     * 商户号
     */
    @XStreamAlias("MerCode")
    @NotBlank
    private String merCode;

    /**
     *商户名
     */
    @XStreamAlias("MerName")
    private String merName;


    /**
     * 账户号
     */
    @XStreamAlias("Account")
    @NotBlank
    private String account;

    /**
     * 消息编号(可选)
     */
    @XStreamAlias("MsgId")
    private String msgId;

    /**
     * 商户请求时间
     */
    @XStreamAlias("ReqDate")
    @NotBlank
    private String reqDate;

    /**
     * 数字签名
     */
    @XStreamAlias("Signature")
    @NotBlank
    private String signature = "0";

}
