/**
 * create by zhangpu
 * date:2015年3月20日
 */
package com.acooly.module.openapi.client.provider.yuejb;


import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.acooly.module.openapi.client.api.message.MessageFactory;
import com.acooly.module.openapi.client.api.message.MessageMeta;
import com.acooly.module.openapi.client.provider.yuejb.domain.YueJBResponse;
import com.acooly.module.openapi.client.provider.yuejb.message.YueJBFreezeResponse;
import com.acooly.module.openapi.client.provider.yuejb.message.YueJBLoanNotify;
import com.acooly.module.openapi.client.provider.yuejb.message.YueJBLoanResponse;
import com.google.common.collect.Maps;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author ouwen
 */
@Component
public class YueJBMessageFactory implements MessageFactory {

	public static Map<String, MessageMeta> metas = Maps.newHashMap();

	static {
		/** 额度冻结 */
		metas.put(YueJBServiceKeys.Freeze.getKey(), new MessageMeta(YueJBFreezeResponse.class));
		/** 放款 */
		metas.put(YueJBServiceKeys.Loan.getKey(), new MessageMeta(YueJBLoanResponse.class,YueJBLoanNotify.class));
	}


	@Override
	public ApiMessage getRequest(String serviceName) {
		return null;
	}

	@Override
	public ApiMessage getResponse(String serviceName) {
		if (metas.get(serviceName) == null || metas.get(serviceName).getResponse() == null) {
			return newInstance(YueJBResponse.class);
		} else {
			return newInstance(metas.get(serviceName).getResponse());
		}
	}

	@Override
	public ApiMessage getNotify(String serviceName) {
		return newInstance(metas.get(serviceName).getAsyncNotify());
	}

	@Override
	public ApiMessage getReturn(String serviceName) {
		return newInstance(metas.get(serviceName).getResponse());
	}

	public <T> T newInstance(Class<T> clazz) {
		try {
			return (T) clazz.newInstance();
		} catch (InstantiationException e) {
			throw new RuntimeException("InstantiationException:" + clazz);
		} catch (IllegalAccessException e) {
			throw new RuntimeException("IllegalAccessException:" + clazz);
		}
	}
}
