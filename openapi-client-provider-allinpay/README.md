<!-- title: 通联通支付SDK -->
<!-- type: gateway -->
<!-- author: zhike -->

## 1. 提供能力

组件的集成模式为标准组件集成方式。直接通过pom文件引入依赖并配置对应的参数即可。

## 2. 使用说明

```xml
<dependency>
  <groupId>com.acooly</groupId>
  <artifactId>openapi-client-provider-allinpay</artifactId>
  <version>4.2.0-SNAPSHOT</version>
</dependency>
```

直接采用通联的SDK，然后扩展异步通知处理
使用方式：
1、在自己工程注入AllinpayApiService

2、此sdk提供通联通单笔代付、交易订单查询、pos交易异步通知、对账下载


3、密钥加载支持方式
   a：通过配置文件配置密钥加载路径；

4、本sdk中pos支付的异步通知地址需要到通联通收银宝后台配置,使用组件接收异步通知的方法如下:
```java
  @Service
  @Slf4j
  public class AllinpayPosPayNotifyService implements NotifyHandler {
      @Override
      public void handleNotify(ApiMessage notify) {
          AllinpayPosPayNotify posPayNotify = (AllinpayPosPayNotify) notify;
          log.info("通联通pos支付异步通知报文:{}", JSON.toJSONString(posPayNotify));
      }
  
      @Override
      public String serviceKey() {
          return AllinpayServiceEnum.POS_PAY_NOTIFY.getCode();
      }
  }
```
5、本sdk支持pos支付异步通知密钥托管方式，如果没有实现此接口，将默认用配置文件中的密钥，具体接口实现如下
```java
    @Component
    @Slf4j
    public class AllinpayLoadKeyStoreService extends AbstractKeyLoadManager<String> implements KeySimpleLoader {
    
        @Override
        public String doLoad(String partnerId) {
            //TODO
            //这个地方可以调用本地的service根据商户号查询此商户对应的密钥返回即可
            return "MD5密钥";
        }
    
        @Override
        public String getProvider() {
            return AllinpayConstants.PROVIDER_NAME_MD5;
        }
    }
```

6、pos异步通知url规则：域名//gateway/notify/allinpayNotify/posPayNotify(必须按照此规则配置，否则组件无法拦截异步通知)

<font color="Hotpink">注：
1、组件为了增强使用性，调用接口的时候只需要组装对应的请求中的body实体就可以了，head和request由组件自动封装
2、针对资金类交易接口，如果对组件抛异常特别敏感的情况下，需要捕获异常，如果发现组件抛ApiClientProcessingException异常，可以当做处理中
   来处理，通过查询接口或异步通知确认订单最终结果！
</font>

7、配置文件示例：
```
##allinpay
acooly.openapi.client.allinpay.enable=true
acooly.openapi.client.allinpay.partnerId=200604000005275
acooly.openapi.client.allinpay.partnerName=20060400000527504
acooly.openapi.client.allinpay.partnerPassword=111111
acooly.openapi.client.allinpay.gatewayUrl=https://test.allinpaygd.com/aipg/ProcessServlet
acooly.openapi.client.allinpay.privateKeyPath=classpath:keystore/allinpay/20060400000527504.p12
acooly.openapi.client.allinpay.privateKeyPassword=111111
acooly.openapi.client.allinpay.publicKeyPath=classpath:keystore/allinpay/allinpay-pds.cer
##pos异步通知验签用的秘钥，如果用托管方式可以不用配置
acooly.openapi.client.allinpay.md5Key=p2zbk2mtoOSArvzxssaaqpsu3gnTUnbG
acooly.openapi.client.allinpay.domain=http://218.70.106.250:8209
acooly.openapi.client.allinpay.connTimeout=10000
acooly.openapi.client.allinpay.readTimeout=30000
acooly.openapi.client.allinpay.filePath=D:/bill
```
