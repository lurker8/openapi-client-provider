package com.acooly.module.openapi.client.provider.allinpay.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * @Auther: zhike
 * @Date: 2018/8/29 14:02
 * @Description:
 */
@Getter
@Setter
@XStreamAlias("QTRANSRSP")
public class AllinpayTradeQueryResponseBody implements Serializable {

    /**
     * 交易详情列表
     */
    @XStreamImplicit(itemFieldName = "QTDETAIL")
    private List<AllinpayTradeQueryResponseDetail> responseDetailList;
}
