/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-16 16:11 创建
 */
package com.acooly.module.openapi.client.provider.fuyou;

import org.springframework.boot.context.properties.ConfigurationProperties;

import static com.acooly.module.openapi.client.provider.fuyou.OpenAPIClientFuyouProperties.PREFIX;


/**
 * @author zhangpu@acooly.cn
 */
@ConfigurationProperties(prefix = PREFIX)
public class OpenAPIClientFuyouProperties {
    public static final String PREFIX = "acooly.openapi.client.fuyou";

    /**
     * 快捷请求地址
     */
    private String quickPayUrl;

    /**
     * 网关请求
     */
    private String netbankPayUrl;

    /**
     * 代付请求
     */
    private String withdrawUrl;

    /**
     * MD5密钥
     */
    private String netbankMd5Key;

    /**
     * 快捷充值密钥
     */
    private String quickMd5Key;


    /**
     * 本系统域名（用于自动生成回调地址）
     */
    private String domain;

    /**
     * 报文中签名字段名称
     */
    private String signatrueParamKey;

    /**
     * 连接超时时间（毫秒）
     */
    private long connTimeout = 10000;

    /**
     * 读超时时间（毫秒）
     */
    private long readTimeout = 30000;

    /**
     * 商户号
     */
    private String partnerId;


    /**
     * 异步通知URL前缀
     */
    private String notifyUrlPrefix = "/";

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getSignatrueParamKey() {
        return signatrueParamKey;
    }

    public void setSignatrueParamKey(String signatrueParamKey) {
        this.signatrueParamKey = signatrueParamKey;
    }

    public long getConnTimeout() {
        return connTimeout;
    }

    public void setConnTimeout(long connTimeout) {
        this.connTimeout = connTimeout;
    }

    public long getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(long readTimeout) {
        this.readTimeout = readTimeout;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getNotifyUrlPrefix() {
        return notifyUrlPrefix;
    }

    public void setNotifyUrlPrefix(String notifyUrlPrefix) {
        this.notifyUrlPrefix = notifyUrlPrefix;
    }

    public String getQuickPayUrl() {
        return quickPayUrl;
    }

    public void setQuickPayUrl(String quickPayUrl) {
        this.quickPayUrl = quickPayUrl;
    }

    public String getNetbankPayUrl() {
        return netbankPayUrl;
    }

    public void setNetbankPayUrl(String netbankPayUrl) {
        this.netbankPayUrl = netbankPayUrl;
    }

    public String getNetbankMd5Key() {
        return netbankMd5Key;
    }

    public void setNetbankMd5Key(String netbankMd5Key) {
        this.netbankMd5Key = netbankMd5Key;
    }

    public String getQuickMd5Key() {
        return quickMd5Key;
    }

    public void setQuickMd5Key(String quickMd5Key) {
        this.quickMd5Key = quickMd5Key;
    }

    public String getWithdrawUrl() {
        return withdrawUrl;
    }

    public void setWithdrawUrl(String withdrawUrl) {
        this.withdrawUrl = withdrawUrl;
    }
}
