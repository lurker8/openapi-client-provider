package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouRequest;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.acooly.module.openapi.client.provider.fuyou.support.FuyouAlias;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/3/19 15:04
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.FUYOU_NETBANK,type = ApiMessageType.Request)
@XStreamAlias("REQUEST")
public class FuyouNetBankRequest extends FuyouRequest{

    /**
     * 商户订单号
     * 客户支付后商户网站产生的一个唯
     * 一的定单号，该订单号应该在相当
     * 长的时间内不重复。富友通过订单
     * 号来唯一确认一笔订单的重复性.
     */
    @FuyouAlias("order_id")
    @NotBlank
    @Size(max = 30)
    private String orderId;

    /**
     * 交易金额
     * 客户支付订单的总金额，一笔订单
     * 一个，以分为单位。不可以为零，
     * 必需符合金额标准。
     */
    @FuyouAlias("order_amt")
    @NotBlank
    @Size(max = 12)
    private String orderAmt;

    /**
     * 支付类型
     * ‘B2C’ – B2C 支付
     * ‘B2B’ – B2B 支付
     * ‘FYCD’ – 预付卡
     * ‘SXF’ – 随心富
     */
    @FuyouAlias("order_pay_type")
    @NotBlank
    private String orderPayType = "B2C";

    /**
     * 页面跳转URL
     * 商户接收支付结果通知地址
     */
    @FuyouAlias("page_notify_url")
    @NotBlank
    @Size(max = 200)
    private String pageNotifyUrl;

    /**
     * 后台通知URL
     * 商户接收支付结果后台通知地址
     */
    @FuyouAlias("back_notify_url")
    @NotBlank
    @Size(max = 200)
    private String backNotifyUrl;

    /**
     * 超时时间
     * 1m-15 天，m：分钟、h：小时、d天、1c 当天有效，
     */
    @FuyouAlias("order_valid_time")
    private String orderValidTime;

    /**
     * '0801020000' - 中国工商银行
     * '0801030000' - 中国农业银行
     * '0801040000' – 中国银行
     * '0801050000' - 中国建设银行
     * '0803010000' - 交通银行
     * '0801000000' - 邮政储蓄
     * '0803080000' - 招商银行
     * '0803100000' - 上海浦东发展银行
     * '0803030000' - 中国光大银行
     * '0803050000' - 中国民生银行
     * '0803020000' - 中信银行
     * '0803060000' - 广发银行
     * '0803040000' - 华夏银行
     * '0803090000' - 兴业银行
     v'0000000000' – 其他银行
     */
    @FuyouAlias("iss_ins_cd")
    @NotBlank
    @Size(max = 10)
    private String issInsCd;


    /**
     * 商品展示网址
     */
    @FuyouAlias("goods_display_url")
    @Size(max = 200)
    private String goodsDisplayUrl;

    /**
     * 备注
     */
    @FuyouAlias("rem")
    @Size(max = 60)
    private String rem;

    /**
     * 版本号
     */
    @FuyouAlias("ver")
    @Size(max = 10)
    @NotBlank
    private String ver = "1.0.1";

    /**
     * 商品名称
     */
    @FuyouAlias("goods_name")
    @Size(max = 60)
    private String goodsName;

    /**
     * MD5 摘要数据
     */
    @FuyouAlias("md5")
    private String md5;

    @Override
    public String getSignStr() {
        StringBuilder signStr = new StringBuilder();
        signStr.append(getMchntCd()+"|"+getOrderId()+"|"+getOrderAmt()+"|"+getOrderPayType()+"|")
                .append(getPageNotifyUrl()+"|"+getBackNotifyUrl()+"|");
        if(Strings.isNotBlank(getOrderValidTime())) {
            signStr.append(getOrderValidTime());
        }
        signStr.append("|"+getIssInsCd()+"|");
        if(Strings.isNotBlank(getGoodsName())) {
            signStr.append(getGoodsName());
        }
        signStr.append("|");
        if(Strings.isNotBlank(getGoodsDisplayUrl())) {
            signStr.append(getGoodsDisplayUrl());
        }
        signStr.append("|");
        if(Strings.isNotBlank(getRem())) {
            signStr.append(getRem());
        }
        signStr.append("|"+getVer()+"|");
        return signStr.toString();
    }
}
