package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouRequest;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/3/19 15:04
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.AGREEMENT_CARD_QUERY,type = ApiMessageType.Request)
@XStreamAlias("REQUEST")
public class FuyouAgreementCardQueryRequest extends FuyouRequest {

    /**
     * 用户编号
     * 商户端用户的唯一编号，即用户 ID
     */
    @XStreamAlias("USERID")
    @NotBlank
    @Size(max = 40)
    private String userId;

    @Override
    public String getSignStr() {
        return getVersion()+"|"+getPartner()+"|"+getUserId()+"|";
    }
}
