package com.acooly.module.openapi.client.provider.fuyou.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author zhike 2018/3/26 18:21
 */
@Getter
@Setter
@XStreamAlias("trans")
public class FuyouWithdrawQueryInfo implements Serializable{

    /**
     * 原请求日期
     * yyyyMMdd
     */
    @XStreamAlias("merdt")
    @Size(max = 8)
    @NotBlank
    private String merDt;

    /**
     * 原请求流水
     * 数字串，当天必须唯一
     */
    @XStreamAlias("orderno")
    @Size(max = 30)
    @NotBlank
    private String orderNo;

    /**
     * 账号
     * 用户账号
     */
    @XStreamAlias("accntno")
    @Size(max = 28)
    @NotBlank
    private String accntNo;

    /**
     * 账户名称
     * 用户账户名称
     */
    @XStreamAlias("accntnm")
    @Size(max = 30)
    @NotBlank
    private String accntNm;

    /**
     * 交易金额
     * 单位：分
     */
    @XStreamAlias("amt")
    @Size(max = 12)
    @NotBlank
    private String amt;

    /**
     * 企业流水号
     * 填写后，系统体现在交易查询和外部通知中
     */
    @XStreamAlias("entseq")
    @Size(max = 32)
    private String entsEq;

    /**
     * 备注
     * 填写后，系统体现在交易查询和外部通知中
     */
    @XStreamAlias("memo")
    @Size(max = 64)
    private String transMemo;

    /**
     * 交易状态
     */
    @XStreamAlias("state")
    @Size(max = 2)
    @NotBlank
    private String state;

    /**
     * 交易结果
     */
    @XStreamAlias("result")
    @Size(max = 8)
    private String result;

    /**
     * 结果原因
     */
    @XStreamAlias("reason")
    @Size(max = 1024)
    private String reason;
}
