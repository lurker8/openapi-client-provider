package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouResponse;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.acooly.module.openapi.client.provider.fuyou.message.dto.FuyouNetBankSynchroQueryInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/3/19 15:51
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.FUYOU_NETBANK_SYNCHRO_QUERY,type = ApiMessageType.Response)
@XStreamAlias("ap")
public class FuyouNetBankSynchroQueryResponse extends FuyouResponse{

    /**
     * 查询响应业务报文
     */
    @XStreamAlias("plain")
    private FuyouNetBankSynchroQueryInfo netBankSynchroQueryInfo;

    /**
     * 摘要数据
     */
    @XStreamAlias("md5")
    private String md5;
}
