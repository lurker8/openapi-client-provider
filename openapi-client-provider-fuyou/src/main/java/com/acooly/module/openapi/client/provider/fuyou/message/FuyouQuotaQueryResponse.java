package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouResponse;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.acooly.module.openapi.client.provider.fuyou.message.dto.FuyouQuotaQueryBankLimitInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import com.thoughtworks.xstream.annotations.XStreamImplicit;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author zhike 2018/3/19 15:51
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.QUOTA_QUERY,type = ApiMessageType.Response)
@XStreamAlias("RESPONSE")
public class FuyouQuotaQueryResponse extends FuyouResponse{

    /**
     * 摘要数据
     */
    @XStreamAlias("SIGN")
    private String sign;

    /**
     * 银行限额列表
     */
    @XStreamImplicit(itemFieldName="BANKLMT")
    private List<FuyouQuotaQueryBankLimitInfo> quotaQueryBankLimitInfo;

    @Override
    public String getSignStr() {
        return getPartner()+"|"+getResponseCode()+"|"+getResponseMsg()+"|";
    }
}
